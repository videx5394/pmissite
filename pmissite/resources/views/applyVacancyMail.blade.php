<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Apply</title>
</head>
<body>
<h2>Apply</h2>
<table style="text-align: center">
    <thead>
    <tr>
        <th>Position : </th>
        <th>{{$position}}</th>
    </tr>
    </thead>
   <tbody>
   </tbody>
</table>
</body>
</html>
