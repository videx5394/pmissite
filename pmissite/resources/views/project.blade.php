@extends('app')

@section('content')
    @include('layouts.pageHead')<!-- /.page-head -->
    <style>
        .project-images img
        {
            height: 350px;
            object-fit: cover;
            width: 100%;
            margin: 2px;
        }
    </style>

    <!-- Overview AREA START -->
    <section class="about-content-area section" style="margin-right: 20px; margin-left: 20px;">
        <div class="container-fluid">
            <div class="project-text col-md-6">
               {!! $data->project->text !!}
            </div>
            <div class="project-images col-md-6">
                @if(isset($data->project->image1_path))
                    <div class="col-md-12">
                        <img src="{{config('app.uploads_location')}}/{{ $data->project->image1_path }}" >
                    </div>
                @endif
                <div class="">
                    @if(isset($data->project->image2_path))
                        <div class="col-md-6" style="padding-right: 2px;">
                            <img src="{{config('app.uploads_location')}}/{{ $data->project->image2_path }}" >
                        </div>
                    @endif
                    @if(isset($data->project->image3_path))
                        <div class="col-md-6" style="padding-left: 2px;">
                            <img src="{{config('app.uploads_location')}}/{{ $data->project->image3_path }}" >
                        </div>
                    @endif
                </div>
                @if(isset($data->project->image4_path))
                    <div class="col-md-12">
                        <img src="{{config('app.uploads_location')}}/{{ $data->project->image4_path }}" >
                    </div>
                @endif
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.about-content-area -->
    <script>

    </script>
@endsection
