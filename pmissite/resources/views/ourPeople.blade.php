@extends('app')

@section('content')

    <style>
        .profile_image {
            width: 200px;
            height: 200px;
            object-fit: cover;
        }

        .linkedInBtn {
            padding: 20px;
            font-size: 14px;
            border-radius: 50%;
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 50px !important;
            min-height: 50px;
        }

        .col-centered {
            float: none;
            margin: 0 auto;
        }
    </style>

    <section class="page-head page-bg" style="background-image: url('{{config('app.uploads_location')}}/{{$data->people_image_path}}')">
        <div class="page-heading-wrapper">
            <h3 class="page-heading">@lang('header.'.$page_name)</h3>
            @if(isset($data->slogan))
                <div class="slogan">
                    {{$data->slogan}}
                </div>
            @endif
        </div> <!-- /.container -->
    </section>


    <!-- /Page Header AREA END -->


    <!-- Our Team AREA START -->
    <section class="our-team section">
        <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                        <div class="section-heading">
                            <h2 class="title"><span>@lang('home.our')</span> @lang('home.people')</h2>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="row">
                    <div style="
                justify-content: center !important;
                display: flex;
                flex-wrap: wrap;">
                        @foreach($data->members as $member)
                            @if($member->member_type == 'OUR PEOPLE')
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <div class="team-each">
                                        <div class="team-member">
                                            <img class="profile_image"
                                                 src="{{config('app.uploads_location')}}/{{$member->image_path}}"
                                                 alt="Team Member">
                                            <div class="member-hover">
                                                <div class="member-social">
                                                    <ul>
                                                        <li><a target="_blank" href="{{$member->linkedIn}}"><i class="fa fa-linkedin"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div><!-- /.member-hover -->
                                        </div> <!-- /.team-members -->
                                        <div class="member-name">
                                            <h4>{{$member->name}}</h4>
                                            <h5>{!!$member->position!!}</h5>
                                        </div><!-- /.member-name -->
                                    </div> <!-- /.team-each -->
                                </div> <!-- /.col -->
                            @endif
                        @endforeach
                    </div>

                    </div><!-- row -->
                </div><!-- container -->
    </section> <!-- /.our-team -->
    <!-- /Our Team AREA END -->
@endsection

