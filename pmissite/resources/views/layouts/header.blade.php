
<style>
    .langs{
        position: relative;
        top: 25px;
        float: right;
        margin-left: 65px;
    }
    .langs li
    {
        display: inline-block;
        position: relative;
        color: white !important;
        margin-right: 5px;
    }

    .langs li a {
        color: white !important;
        border: 1px solid white;
        padding: 3px;
    }

    .langs.mobile {
        top: 0;
    }

    .langs.mobile li a {
        color: gray !important;
    }

    .langs li a.active {
        color: #f9ba48 !important;
        border: 1px solid #f9ba48;
        cursor: default;
    }
</style>
<!-- Start  Logo & Menu  -->
<div class="navbar navbar-default navbar-top">
    <div class="navbar-inner" >
        <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <div class="logo">
                <a class="navbar-brand" href="{{route('home')}}">
                    <img alt="Logo" src="/assets/images/logo.svg">
                    <div class="logo_text">
                        <span class="text"><span class="firstLetter">P</span>
                            <span class="logo_letter">R</span>
                            <span class="logo_letter">O</span>
                            <span class="logo_letter">J</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">C</span>
                            <span class="logo_letter">T</span>
                        </span>
                        <span class="text"><span class="firstLetter">M</span>
                            <span class="logo_letter">A</span>
                            <span class="logo_letter">N</span>
                            <span class="logo_letter">A</span>
                            <span class="logo_letter">G</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">M</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">N</span>
                            <span class="logo_letter">T</span>
                        </span>
                        <span style="left: 2px;" class="text"><span class="firstLetter">I</span>
                            <span class="logo_letter">M</span>
                            <span class="logo_letter">P</span>
                            <span class="logo_letter">L</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">M</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">N</span>
                            <span class="logo_letter">T</span>
                            <span class="logo_letter">A</span>
                            <span class="logo_letter">T</span>
                            <span class="logo_letter">I</span>
                            <span class="logo_letter">O</span>
                            <span class="logo_letter">N</span>
                        </span>
                        <span  class="text"><span class="firstLetter">S</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">R</span>
                            <span class="logo_letter">V</span>
                            <span class="logo_letter">I</span>
                            <span class="logo_letter">C</span>
                            <span class="logo_letter">E</span>
                            <span class="logo_letter">S</span>
                        </span>
                    </div>
                </a>
            </div> <!-- /.logo -->
        </div>
        <div class="navbar-collapse collapse">
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{route('home')}}">@lang('header.home')</a></li>
                <li>
                    <a href="#">@lang('header.who_we_are')</a>
                    <ul class="dropdown">
                        <li><a href="{{route('about_us')}}">@lang('header.about_us')</a></li>
                        <li><a href="#">@lang('header.our_team')</a>
                            <ul class="sup-dropdown">
                                <li><a href="{{route('our_leadership')}}">@lang('header.our_leadership')</a></li>
                                <li><a href="{{route('our_people')}}">@lang('header.our_people')</a></li>
                                <li><a href="{{route('our_expert_witnesses')}}">@lang('header.our_expert_witnesses')</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('home')}}#testimonial"> @lang('header.our_clients')</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">@lang('header.what_we_do')</a>
                    <ul class="dropdown">
                        <li><a href="#" style="cursor: unset;">@lang('header.PSS')</a>
                            <ul class="sup-dropdown">
                                <li><a href="{{route('PSACE')}}">@lang('header.PSACE')</a></li>
                                <li><a href="{{route('PAS')}}">@lang('header.PAS')</a></li>
                                <li><a href="{{route('QSACA')}}">@lang('header.QSACA')</a></li>
                                <li><a href="{{route('CCAS')}}">@lang('header.CCAS')</a></li>
                                <li><a href="{{route('timekeeping')}}">@lang('header.timekeeping')</a></li>
                                <li><a href="{{route('RAA')}}">@lang('header.RAA')</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="{{route('projects')}}">@lang('header.projects')</a></li>
                <li><a href="{{route('vacancy')}}">@lang('header.vacancy')</a></li>
                <li><a href="{{route('contact_us')}}">@lang('header.contact_us')</a></li>
                <div class="langs">
                    <li><a class="@if(Session::get('locale')=='az') active @endif" href="{{ route('language',['lang'=> 'az']) }}">AZ</a></li>
                    <li><a class="@if(Session::get('locale')=='en') active @endif" href="{{ route('language',['lang'=> 'en']) }}">EN</a></li>
                    <li><a class="@if(Session::get('locale')=='ru') active @endif" href="{{ route('language',['lang'=> 'ru']) }}">RU</a></li>
                </div>
            </ul>
            <!-- End Navigation List -->
        </div>
        <!-- /.navbar-collapse -->
    </div> <!-- /.container -->

    <!-- Mobile Menu Start -->
    <ul class="wpb-mobile-menu">
        <li><a href="{{route('home')}}">@lang('header.home')</a></li>
        <li>
            <a href="#">@lang('header.who_we_are')</a>
            <ul class="dropdown">
                <li><a href="{{route('about_us')}}">@lang('header.about_us')</a></li>
                <li><a href="#">@lang('header.our_team')</a>
                    <ul class="sup-dropdown">
                        <li><a href="{{route('our_leadership')}}">@lang('header.our_leadership')</a></li>
                        <li><a href="{{route('our_people')}}">@lang('header.our_people')</a></li>
                        <li><a href="{{route('our_expert_witnesses')}}">@lang('header.our_expert_witnesses')</a></li>
                    </ul>
                </li>
                <li><a href="{{route('home')}}#testimonial">@lang('header.our_clients')</a></li>
            </ul>
        </li>
        <li>
            <a href="#">@lang('header.what_we_do')</a>
            <ul class="dropdown">
                <li><a href="#" style="cursor: unset;">@lang('header.PSS')</a>
                    <ul class="sup-dropdown">
                        <li><a href="{{route('PSACE')}}">@lang('header.PSACE')</a></li>
                        <li><a href="{{route('PAS')}}">@lang('header.PAS')</a></li>
                        <li><a href="{{route('QSACA')}}">@lang('header.QSACA')</a></li>
                        <li><a href="{{route('CCAS')}}">@lang('header.CCAS')</a></li>
                        <li><a href="{{route('timekeeping')}}">@lang('header.timekeeping')</a></li>
                        <li><a href="{{route('RAA')}}">@lang('header.RAA')</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="{{route('projects')}}">@lang('header.projects')</a></li>
        <li><a href="{{route('vacancy')}}">@lang('header.vacancy')</a></li>
        <li><a href="{{route('contact_us')}}">@lang('header.contact_us')</a></li>
        <div class="langs mobile">
            <li><a class="@if(Session::get('locale')=='az') active @endif" href="{{ route('language',['lang'=> 'az']) }}">AZ</a></li>
            <li><a class="@if(Session::get('locale')=='en') active @endif" href="{{ route('language',['lang'=> 'en']) }}">EN</a></li>
            <li><a class="@if(Session::get('locale')=='ru') active @endif" href="{{ route('language',['lang'=> 'ru']) }}">RU</a></li>
        </div>
    </ul> <!-- /.wpb-mobile-menu -->
</div> <!-- /.navbar -->
<!-- End Header Logo & Naviagtion -->
