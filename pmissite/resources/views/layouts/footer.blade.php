<footer class="footer">

    <div class="copyright">
        <div class="contact-social">
            <ul>
                <li>
                    <a target="_blank" href="{{Session::get('contact_info')['linkedin']}}">
                        <i class="fa normal-show fa-linkedin"></i>
                        <i class="fa hover-show fa-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="container">

            PMIS &copy; 2020 @lang('home.all_rights_reserved')
        </div>
    </div> <!-- /.copyright -->
</footer> <!-- /.footer -->
