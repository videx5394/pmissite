<section class="page-head page-bg" style="background-image: url('{{config('app.uploads_location')}}/{{$data->image_path}}')">
    <div class="page-heading-wrapper">
        <h3 class="page-heading">@lang('header.'.$page_name)</h3>
        @if(isset($data->slogan))
            <div class="slogan">
                {{$data->slogan}}
            </div>
        @endif
    </div> <!-- /.container -->
</section>
