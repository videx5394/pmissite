@extends('app')

@section('content')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @include('layouts.pageHead')<!-- /.page-head -->
    <style>

        .projectWrapper
        {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .project
        {
            border: 1px solid #cbcaca;
            margin: 7px;
            padding: 20px;
            transition: transform .2s;
            cursor: pointer;
        }
        .project:hover {
            transform: scale(1.1);
            box-shadow: -1px 0 10px 1px #e7e7e7;
            z-index: 20;
            background-color: white;
        }
        .location {
            font-size: 17px;
            margin-right: 5px;
            color: #00abe5;
        }
        .small_image
        {
            width: 100px;
            height: 100px;
            object-fit: cover;
            margin: 10px;
            float: left;
        }
    </style>

    <!-- Overview AREA START -->
    <section class="about-content-area section">
        <div class="">
            <div class="projectWrapper row-fluid">
                @foreach($data->projects as $key => $project)
                    <div class="project col-xs-12 col-sm-6 col-md-6" onclick="openProject('{{route('project',$project->id)}}')">
                        <img class="small_image" src="{{config('app.uploads_location')}}/{{$project->small_image_path}}"
                             alt="small image">
                        <h4>{{$project->title}}</h4>
                        <p><i class="fa fa-map-marker location" aria-hidden="true"></i>{{$project->location}}</p>
                        <h5>{{$project->date}}</h5>
                    </div>
                    @endforeach
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.about-content-area -->
    <script>
        function openProject(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
@endsection
