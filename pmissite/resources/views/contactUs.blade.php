@extends('app')

@section('content')

    <style>
        .form_errors li{
            color: #ff000087;
            font-size: 16px;
            margin: 5px;
        }
        ::placeholder {
            color: rgba(0, 0, 0, 0.5) !important;
            opacity: 1; /* Firefox */
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(0, 0, 0, 0.5) !important;
        }

        ::-ms-input-placeholder { /* Microsoft Edge */
            color: rgba(0, 0, 0, 0.5) !important;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>



    @include('layouts.pageHead')
    <!-- /Page Header AREA END -->


    <section class="contact-us section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6" style="padding-top: 32px;">
                    {!! $data->text !!}
                    <div style="display: none;" class="lets-contact">
                        <div class="contact-social">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa normal-show fa-facebook"></i>
                                        <i class="fa hover-show fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa normal-show fa-twitter"></i>
                                        <i class="fa hover-show fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa normal-show fa-google-plus"></i>
                                        <i class="fa hover-show fa-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa normal-show fa-pinterest-p"></i>
                                        <i class="fa hover-show fa-pinterest-p"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa normal-show fa-youtube"></i>
                                        <i class="fa hover-show fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div> <!-- /.contact-social -->
                    </div> <!-- /.lets-contact -->
                </div> <!-- /.col- -->
                <div class="col-xs-12 col-sm-6">
                    <div class=" ">
                        <ul class="form_errors">

                        </ul>
                    </div>
                    <div class="border-in">
                        <div class="comm-title">
                            <h3>@lang('contactUs.get_in_touch')</h3>
                        </div>
                        <div class="contact-form">
                            <form id="contact-form" method="post" action="http://themeforsale.com/template/preview-template/preview-template-v1.10/assets/contact-script/contact.php">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="@lang('contactUs.name')" required="required" data-error="Write your name">
                                        </div>
                                    </div> <!-- /.col- -->
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" placeholder="@lang('contactUs.email')" required="required" data-error="Write your valid email address">
                                        </div>
                                    </div> <!-- /.col- -->
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="subject" class="form-control" placeholder="@lang('contactUs.subject')">
                                        </div>
                                    </div> <!-- /.col- -->
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <textarea name="comment" class="form-control" placeholder="@lang('contactUs.message')" required="required" data-error="Write your comment"></textarea>
                                        </div>
                                    </div> <!-- /.col- -->
                                    <div class="form-group col-xs-12">
                                        <div class="form-btn">
                                            <input type="submit" name="submit" value="@lang('contactUs.send')" onclick="contactUs()" class="contact-form-btn">
                                        </div>
                                    </div>
                                    <div class="messages"></div>
                                </div> <!-- /.row -->
                            </form>
                        </div> <!-- /.contact-form -->
                    </div>
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.contact-us -->

    <script>
        let contactUs = async () => {

            addLoading($('form'));

            const data = {
                'name' : $('[name="name"]').val(),
                'subject' : $('[name="subject"]').val(),
                'comment' : $('[name="comment"]').val(),
                'email' : $('[name="email"]').val()
            };

            const response = await fetch("{{route('contactUsMail')}}", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json',
                    'Accept':'application/json'
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if(response.status === 200)
            {
                Swal.fire({
                    title: "@lang('contactUs.form_success_title')",
                    text: "@lang('contactUs.form_success_message')",
                    icon: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    window.location.href = '/';
                })
            }
            else if(response.status === 422 )
            {
                const result = await response.json();
                const errors = result.errors;
                $('.form_errors').html('');
                for (fieldName in errors)
                {
                    let field = errors[fieldName];
                    for (idx in field)
                    {
                        let error = field[idx].replace(fieldName,"'"+$("[name='"+fieldName+"']").attr('placeholder')+"'");
                        $('.form_errors').append(`<li>${error}</li>`);
                    }
                }
            }
            else
            {
                Swal.fire({
                    title: "@lang('contactUs.form_error_title')",
                    text: "@lang('contactUs.form_error_message')",
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
            removeLoading($('form'));
        };
    </script>
@endsection

