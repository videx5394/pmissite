@extends('app')

@section('content')

    @include('layouts.pageHead')<!-- /.page-head -->
    <!-- /Page Header AREA END -->


    <!-- Overview AREA START -->
    <section class="about-content-area section">
        <div style="display: inline-block">
            <div class="col-md-offset-3 col-md-6">
                {!! $data->text !!}
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.about-content-area -->
@endsection

