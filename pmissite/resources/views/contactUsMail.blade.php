<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Concat Us</title>
</head>
<body>
<h2>Concat Us</h2>
<table style="text-align: center">
    <thead>
    <tr>
        <th>Field</th>
        <th>Value</th>
    </tr>
    </thead>
   <tbody>
   <tr>
       <td>Name</td>
       <td>{{$request['name']}}</td>
   </tr>
   <tr>
       <td>Email</td>
       <td>{{$request['email']}}</td>
   </tr>
   <tr>
       <td>Subject</td>
       <td>{{$request['subject']}}</td>
   </tr>
   <tr>
       <td>Comment</td>
       <td>{{$request['comment']}}</td>
   </tr>
   </tbody>
</table>
</body>
</html>
