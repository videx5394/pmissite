@extends('app')

@section('content')

    <style>
        .profile_image {
            width: 200px;
            height: 200px;
            object-fit: cover;
        }

        .linkedInBtn {
            padding: 20px;
            font-size: 14px;
            border-radius: 50%;
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 50px !important;
            min-height: 50px;
        }

        .col-centered {
            float: none;
            margin: 0 auto;
        }

        .no_border
        {
            border: 0;
        }

        .image_circle
        {
            border-radius: 50%;
        }

        .team_name
        {
            font-size: 16px;
            color: #26476c;
            font-weight: 700;
        }
    </style>

    @include('layouts.pageHead')<!-- /.page-head -->
    <!-- /Page Header AREA END -->


    <!-- Our Team AREA START -->
    <section class="our-team section">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                    <div class="section-heading">
                        <h2 class="title"><span>Our</span> TEAM</h2>
                    </div>
                </div>
            </div> <!-- /.row -->
            <div class="row">

                <div style="
                justify-content: center !important;
                display: flex;
                flex-wrap: wrap;">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="team-each no_border">
                            <div class="team-member">
                                <a href="{{route('our_leadership')}}">
                                    <img class="profile_image image_circle"
                                         src="{{config('app.uploads_location')}}/{{$data->leadership_image_path}}"
                                         alt="Team Member">
                                </a>
                            </div> <!-- /.team-members -->
                            <div class="member-name">
                                <h4 class="team_name">@lang('header.our_leadership')</h4>
                            </div><!-- /.member-name -->
                        </div> <!-- /.team-each -->
                    </div> <!-- /.col -->
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="team-each no_border">
                            <div class="team-member">
                                <a href="{{route('our_people')}}">
                                    <img class="profile_image image_circle"
                                         src="{{config('app.uploads_location')}}/{{$data->people_image_path}}"
                                         alt="Team Member">
                                </a>

                            </div> <!-- /.team-members -->
                            <div class="member-name">
                                <h4 class="team_name">@lang('header.our_people')</h4>
                            </div><!-- /.member-name -->
                        </div> <!-- /.team-each -->
                    </div> <!-- /.col -->
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="team-each no_border">
                            <div class="team-member">
                                <a href="{{route('our_expert_witnesses')}}">
                                    <img class="profile_image image_circle"
                                         src="{{config('app.uploads_location')}}/{{$data->expert_image_path}}"
                                         alt="Team Member">
                                </a>

                            </div> <!-- /.team-members -->
                            <div class="member-name">
                                <h4 class="team_name">@lang('header.our_expert_witnesses')</h4>
                            </div><!-- /.member-name -->
                        </div> <!-- /.team-each -->
                    </div> <!-- /.col -->
                </div>
            </div>
        </div>
    </section> <!-- /.our-team -->
    <!-- /Our Team AREA END -->
@endsection

