@extends('app')

@section('content')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @include('layouts.pageHead')<!-- /.page-head -->
    <style>
        textarea {
            resize: none;
        }
        .vacancyWrapper
        {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .vacancy
        {
            border: 1px solid #cbcaca;
            margin: 7px;
            padding: 20px;
            transition: transform .2s;
            cursor: pointer;
            padding-top: 30px;
            padding-bottom: 30px;
        }
        .vacancy:hover {
            transform: scale(1.1);
            box-shadow: -1px 0 10px 1px #e7e7e7;
            z-index: 20;
            background-color: white;
        }
        .location {
            font-size: 17px;
            margin-right: 5px;
            color: #00abe5;
        }
        .modal {
            z-index: 9999999;
        }
        .form-row
        {
            margin-top: 20px;
            font-size: 15px !important;
        }
        .form-control[readonly]
        {
            border: 0;
            background-color: #d3d3d32b;
            font-weight: bold;
        }
        [hidden] {
            display: none !important;
        }
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
        .filename
        {
            margin-left: 10px;
        }

        .form-control:focus {
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, 0);
        }
    </style>

    <!-- Overview AREA START -->
    <section class="about-content-area section">
        <div class="">
            <div class="vacancyWrapper row-fluid">
                @foreach($data->vacancies as $key => $vacancy)
                    <div class="vacancy col-xs-12 col-sm-6 col-md-2" data-toggle="modal" data-target="#vacancyModal{{$key}}">
                        <h4>{{$vacancy->title}}</h4>
                        <p><i class="fa fa-map-marker location" aria-hidden="true"></i>{{$vacancy->location}}</p>
                    </div>
                    <div class="modal fade" id="vacancyModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="vacancyModal{{$key}}" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">{{$vacancy->title}}</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="">
                                        <form class="vacancyForm" method="post">
                                            <div class="form-row row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 col-form-label">@lang('vacancy.position')</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="position"  readonly class="form-control"   value="{{$vacancy->position}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 col-form-label">@lang('vacancy.age')</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="age"  readonly class=" form-control"   value="{{$vacancy->age}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="form-row row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 col-form-label">@lang('vacancy.education')</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="education"  readonly class=" form-control"   value="{{$vacancy->education}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 col-form-label">@lang('vacancy.experience')</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="experience"  readonly class=" form-control"   value="{{$vacancy->experience}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 col-form-label">@lang('vacancy.salary')</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="salary"  readonly class=" form-control"   value="{{$vacancy->salary}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 col-form-label">@lang('vacancy.schedule')</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" name="schedule"  readonly class=" form-control"   value="{{$vacancy->schedule}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-row row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 col-form-label">@lang('vacancy.published_on')</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="published_on"  readonly class=" form-control"   value="{{$vacancy->published_on}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 col-form-label">@lang('vacancy.expired_on')</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="expired_on"  readonly class=" form-control"   value="{{$vacancy->expired_on}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 col-form-label">@lang('vacancy.meal')</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="meal"  readonly class=" form-control"   value="@lang('vacancy.'.$vacancy->meal)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 col-form-label">@lang('vacancy.transport')</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="transport"  readonly class=" form-control"   value="@lang('vacancy.'.$vacancy->transport)">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row row"></div>
                                            <div class="form-row row">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 col-form-label">@lang('vacancy.description')</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" name="description"  readonly class=" form-control">{{$vacancy->description}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row row">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 col-form-label">@lang('vacancy.requirements')</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" name="requirements"  readonly class=" form-control" >{{$vacancy->requirements}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row row">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 col-form-label">@lang('vacancy.attach')</label>
                                                        <div class="col-sm-8">
                                                            <span class="btn btn-default btn-file">
                                                                @lang('vacancy.download') <input name="attachment" onchange="attachmentChanged(this)" type="file">
                                                            </span>
                                                            <span name="filename" class=""> </span>
{{--                                                            <textarea type="text" name="attach"  readonly class=" form-control"   value="{{$vacancy->attach}}"></textarea>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button onclick="applyVacancy('{{$vacancy->position}}')" type="button" class="btn btn-primary">@lang('vacancy.apply')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.about-content-area -->
@endsection

<script>
    function attachmentChanged(element) {
        $('[name="filename"]').text($(element).val());
    }

    async function sendData(url, data) {
        const formData  = new FormData();

        for(const name in data) {
            formData.append(name, data[name]);
        }

        const response = await fetch(url, {
            method: 'POST',
            body: formData
        });
    }

    let formData = new FormData();
    async function applyVacancy(position)
    {
        var cv = $('[name="attachment"]')[0].files[0];
        if(cv)
        {
            $('.modal-footer button').attr('disabled',true);
            formData.append('cv',cv);
            formData.append('position', position);
            const response = await fetch('{{route('applyVacancy')}}', {
                method: 'POST',
                body: formData
            });
            if(response.status == 200)
            {
                $('[data-dismiss="modal"]').click();
                Swal.fire({
                    title: "@lang('vacancy.form_success_title')",
                    text: "@lang('vacancy.form_success_message')",
                    icon: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    window.location.href = '/';
                })
            }
            else if(response.status == 400)
            {
                Swal.fire({
                    title: "@lang('vacancy.form_error_title')",
                    text: "@lang('vacancy.form_error_message')",
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
            $('.modal-footer button').attr('disabled',false)
        }
        else
        {
            return;
        }
    }
</script>
