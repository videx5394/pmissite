<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<html lang="en" class="no-js"> <![endif]-->
<html lang="en">


<head>
    @include('layouts.head')
</head>

<body>

<div id="loader">
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
</div>

<!-- Full Body Container -->
<div id="all-area" class="all-section">

    <!-- Header Area START -->
    <header class="clearfix header" data-spy="affix" data-offset-top="60">
        @include('layouts.header')
    </header> <!-- /.header -->
    <!--Header Area END -->
<div id="mainContent">
    @yield('content')
</div>

<!-- Footer AREA START -->
@include('layouts.footer')
<!-- /Footer AREA END -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
</div> <!-- /#all-area -->
<!-- End Full Body / all-area -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/assets/js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!-- jQuery Migrate -->
<script src="/assets/js/jquery-migrate-3.0.0.min.js"></script>
<!-- Bootstrap -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- Contact Form Script-->
<script src="/assets/contact-script/validator.js"></script>
<script src="/assets/contact-script/contact.js"></script>
<!-- Owl Carousel -->
{{--<script src="/assets/js/owl.carousel.min.js"></script>--}}
<!-- Slick Nav -->
<script src="/assets/js/jquery.slicknav.js"></script>
<!-- Slider Touch to work -->
<script src="/assets/js/jquery.mobile.custom.min.js"></script>
<!-- FancyBox -->
<script src="/assets/js/jquery.fancybox.min.js"></script>
<!-- Isotope -->
<script src="/assets/js/isotope.pkgd.min.js"></script>
<!-- Counterup -->
<script src="/assets/js/jquery.counterup.min.js"></script>
<script src="/assets/js/waypoints.min.js"></script>
<!-- ProgressBar -->
<script src="/assets/js/jquery.lineProgressbar.js"></script>
<!-- Slick Slider -->
<script src="/assets/js/slick.min.js"></script>
<!-- particles.js -->
<script src="/assets/js/particles.min.js"></script>
<script src="/assets/js/particles-config.js"></script>
<!-- Google Map -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js" async defer integrity="sha256-oQaw+JJuUcJQ9QVYMcFnPxICDT+hv8+kuxT2FNzTGhc=" crossorigin="anonymous"></script>

<!-- Main / Custom JS  -->
<script src="/assets/js/custom.js"></script>
</body>

<script>
    function addLoading(element) {
        element.fadeTo("400", "0.6").block({
            message: null,
            overlayCSS: {
                backgroundSize: "40px 40px",
                opacity: 0.2
            }
        });
    }


    function removeLoading(element) {
        element.stop(!0).css("opacity", "1").unblock();
    }

</script>

</html>
