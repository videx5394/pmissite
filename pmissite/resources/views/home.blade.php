@extends('app')

@section('content')


    <style>
        .what_we_do
        {
            position: relative;
            background-attachment: fixed;
        }

        .what_we_do .background
        {
            background-color: #223e5c;
            top: 0;
            opacity: 0.9;
            left: 35%;
            content: '';
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 1;
            transform: skewX(-20deg);
        }

        .what_we_do .text-inner
        {
            z-index: 999;
            padding: 50px;
            /*background: linear-gradient(90deg, rgba(255, 255, 255, 0.05) 0%, rgba(38, 71, 108, 0) 39%, rgba(38, 71, 108, 0) 100%);*/
        }
    </style>
    <!-- Start Main Slider -->
    <section id="main-slider">
        <!-- Carousel -->
        <div id="main-slide" class="main-slider carousel slide" data-ride="carousel">
            <!-- Carousel inner -->
            <div class="carousel-inner">
                <div class="item active">
                    <img class="home-img img-responsive" src="{{config('app.uploads_location')}}/{{$data->banner->image_path}}" alt="slider">
                    <div class="container">
                        <div class="slider-content">
                            <div class="container">
                                <div class="animated4">
                                    {!! $data->banner->text !!}
                                </div>
                                <div class="animated5 slogan">
                                    {!! $data->banner->slogan !!}
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.container -->
                </div> <!-- /.item -->
            </div>
            <!-- Carousel inner end-->

        </div>
        <!-- /carousel -->
    </section> <!-- /.main-slider -->
    <!-- End Main Slider -->

    <section class="about section">
        <div class="container-fluid">
            <div class="col-sm-offset-2 col-sm-7">
                {!! $data->about->text !!}
            </div>
        </div>
    </section>


    <!-- Video AREA START -->
    <section class="vid cbiz-bg" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                    <div class="section-heading">
                        <div class="video-content">
                            <a href="https://www.youtube.com/embed/NC_vbRp4GT0?autoplay=1" class="video-btn various fancybox.iframe">
                                <img src="assets/images/icons/video.png" alt="Video Icon">
                            </a>
                            <h4>Watch The Video</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                        </div>
                    </div> <!-- /.section-heading -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.vid -->
    <!-- /Video AREA END -->


    <!-- Testimonial AREA START -->
    <section class="testimonial section" id="testimonial">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                    <div class="section-heading">
                        <h2 class="title"><span>@lang('home.our')</span> @lang('home.clients')</h2>
                    </div>
                </div>
            </div> <!-- /row -->
            <div class="row">
                <div class="">
                    <div class="row">
                        <div class="testi-slider" style="width: 100%">
                            <div class="slider slider-nav">
                                @foreach($data->clients as $client)
                                    <div class="item">
                                        <div class="testi-item">
                                            <div class="">
                                                <img src="{{config('app.uploads_location')}}/{{ $client->image_path }}" alt="client photo">
                                            </div> <!-- /. -->
                                        </div> <!-- /.testi-item -->
                                    </div>
                                @endforeach
                            </div> <!-- /.slider-nav -->
                            </div> <!-- /.slider-for -->
                        </div> <!-- /.testi-slider -->
                    </div> <!-- /.row -->
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.testimonial -->
    <!-- /Testimonial AREA END -->


@endsection

