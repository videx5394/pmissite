<?php

return [
    'form_error_message' => 'Your message has not been sent because of internal error. Please try again later',
    'form_error_title' => 'Error',
    'form_success_message' => 'Your message has been sent. Our service manager will contact you soon',
    'form_success_title' => 'Thank you for your opinion',
    'get_in_touch' => 'GET IN TOUCH WITH US',
    'name' => 'Name',
    'email' => 'Email',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send'
];
