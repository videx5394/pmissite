<?php

return [
    'read_more' => 'Read more',
    'our'=>'Our',
    'clients'=>'CLIENTS',
    'leadership'=>'LEADERSHIP',
    'people'=>'TEAM',
    'expert_witnesses'=> 'EXPERT WITNESSES',
    'all_rights_reserved' => 'All rights reserved.',
];
