<?php

return [
    'home'=>'Home',
    'who_we_are' => 'WHO WE ARE',
    'what_we_do' => 'WHAT WE DO',
    'projects' => 'Projects',
    'vacancy' => 'Vacancy',
    'contact_us' => 'Contact us',
    'language' => 'Language',
    'about_us' => 'ABOUT  PMIS',
    'our_team' => 'OUR TEAM',
    'our_leadership' => 'OUR LEADERSHIP',
    'our_people' => 'OUR TEAM',
    'our_expert_witnesses' => 'OUR EXPERT WITNESSES',
    'our_clients' => 'OUR CLIENTS',
    'PSS' => 'PROJECT SERVICES SOLUTIONS',
    'PSACE' => 'TENDERING / COST ESTIMATION  ',
    'PAS' => 'PLANNING / SCHEDULING',
    'QSACA' => 'QUANTITY SURVEYING / CONTRACT ADMINISTRATION',
    'CCAS' => 'COST CONTROL / SUBCONTRACTING',
    'timekeeping' => 'HISTORICAL BENCHMARKING / SITE TIMEKEEPING',
    'RAA' => 'REPORTING / VISUALIZATION',
];
