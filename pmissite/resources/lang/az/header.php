<?php

return [
    'home'=>'ANA SƏHİFƏ',
    'who_we_are' => 'BİZ KİMİK',
    'what_we_do' => 'XİDMƏTLƏR',
    'projects' => 'LAYİHƏLƏR',
    'vacancy' => 'VAKANSİYALAR',
    'contact_us' => 'ƏLAQƏ',
    'language' => 'Language',
    'about_us' => 'HAQQIMIZDA',
    'our_team' => 'BİZİM KOMANDA',
    'our_leadership' => 'İDARƏ HEYƏTİ',
    'our_people' => 'KOMANDAMIZ',
    'our_expert_witnesses' => 'EKSPERTLƏRİMİZ',
    'our_clients' => 'SİFARİŞÇİLƏR',
    'PSS' => 'LAYİHƏ SERVİS XİDMƏTLƏRİ',
    'PSACE' => 'TENDER XİDMƏTİ / QİYMƏTLƏNDİRMƏ',
    'PAS' => 'PLANLAMA',
    'QSACA' => 'İŞ HƏCMLƏRİNƏ NƏZARƏT / MÜQAVİLƏLƏRƏ NƏZARƏT',
    'CCAS' => 'XƏRCLƏRƏ NƏZARƏT /SUB-PODRATÇI ŞİRKƏTLƏRƏ NƏZARƏ',
    'timekeeping' => 'MÜQAYISƏLI TƏHLİL / İŞ SAATLARINA NƏZARƏT',
    'RAA' => 'LAYİHƏ HESABATLARI / VİZUAL TƏQDİMAT',
];
