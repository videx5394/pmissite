<?php

return [
    'read_more' => 'Ətraflı Oxu',
    'our'=>'Bizim',
    'clients'=>'SİFARİŞÇİLƏR',
    'leadership'=>'İDARƏ HEYƏTİ',
    'people'=>'KOMANDA',
    'expert_witnesses'=> 'EKSPERTLƏRİMİZ',
    'all_rights_reserved' => 'Bütün hüquqlar qorunur.',
];
