<?php

return [
    'form_error_message' => 'Daxili xətaya görə mesajınız göndərilmədi',
    'form_error_title' => 'Xəta',
    'form_success_message' => 'Mesajınız göndərildi',
    'form_success_title' => 'Təşəkkür edirik',
    'get_in_touch' => 'BIZIMLƏ ƏLAQƏ SAXLAYIN',
    'name' => 'Ad',
    'email' => 'Soyad',
    'subject' => 'Mövzu',
    'message' => 'Mesaj',
    'send' => 'Göndər'
];
