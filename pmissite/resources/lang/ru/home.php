<?php

return [
    'read_more' => 'Прочитать',
    'our'=>'Наши',
    'clients'=>'КЛИЕНТЫ',
    'leadership'=>'ЛИДЕРЫ',
    'people'=>'КОМАНДА',
    'expert_witnesses'=> 'ЭКСПЕРТЫ',
    'all_rights_reserved' => 'Все права защищены.',
];
