<?php

return [
    'position'=>'Позиция',
    'age'=>'Возраст',
    'education'=>'Возраст',
    'experience'=>'Опыт работы',
    'salary'=>'Зарплата',
    'schedule'=>'График работы',
    'location'=>'Расположение',
    'published_on'=>'Опубликован',
    'expired_on'=>'Истекает',
    'meal'=>'Питание',
    'transport'=>'Транспорт',
    'description'=>'Описание работы',
    'requirements'=>'Минимальные требования',
    'attach'=>'Прикрепите свое резюме',
    'download'=>'Загрузить',
    'apply'=>'Применять',
    'yes'=> 'За счет компании',
    'no'=> 'За свой счет',
    'form_error_message' => 'Ваше CV не было отправлено из-за системной ошибки. Пожалуйста попробуйте позже.',
    'form_error_title' => 'Ошибка',
    'form_success_message' => 'Ваше CV было успешно отправлено.',
    'form_success_title' => 'Спасибо',
];
