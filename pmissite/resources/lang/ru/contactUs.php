<?php

return [
    'form_error_message' => 'Ваше сообщение не было отправлено из-за системной ошибки. Пожалуйста попробуйте позже.',
    'form_error_title' => 'Ошибка',
    'form_success_message' => 'Ваше сообщение было успешно отправлено.',
    'form_success_title' => 'Спасибо',
    'get_in_touch' => 'СВЯЖИТЕСЬ С НАМИ',
    'name' => 'Имя',
    'email' => 'Эл. Почта',
    'subject' => 'Тема',
    'message' => 'Сообщение',
    'send' => 'Отправить'
];
