/* ----------------- Start JS Document ----------------- */

async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            //'Content-Type': 'application/json'
            //'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *client
        body: data // body data type must match "Content-Type" header
    });
    return await response.json(); // parses JSON response into native JavaScript objects
}

(function($) {
    "use strict";

    $(document).ready(function() {

        function adjustHeight(o){
            o.style.height = "1px";
            o.style.height = (25+o.scrollHeight)+"px";
        }

        $(window).on('shown.bs.modal', function() {
            $('.vacancyForm textarea').each(function(){
                adjustHeight($(this)[0]);
            });
        });

        //Nav Menu
        $(".nav > li:has(ul)").addClass("drop");
        $(".nav > li.drop > ul").addClass("dropdown");
        $(".nav > li.drop > ul.dropdown ul").addClass("sup-dropdown");

        $('.navbar-nav li a').each(function () {
            let href = window.location.href;
            if(href === $(this).attr('href'))
            {
                if($(this).closest('.drop').length>0)
                {
                    $(this).closest('.drop').find('a').addClass('active');
                }
                else
                {
                    $(this).addClass('active');
                }
            }
        });



        /* Animated skills Bars */
        function DemoProgressbars(){

            var prog;

            $('.bar-progres').each(function() {
                prog = $(this);
                prog.LineProgressbar({
                    percentage: prog.data('percent'),
                    fillBackgroundColor: prog.data('color'),
                    duration: 2500
                });
            });

        }


        $('.progressbar-start').waypoint(function(){
            DemoProgressbars();
        },{
            offset: '100%',
            triggerOnce: true
        });


        // Video Popup
        $(".various").fancybox({
            maxWidth    : 800,
            maxHeight   : 600,
            fitToView   : false,
            width       : '70%',
            height      : '70%',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none'
        });

        $("[data-fancybox]").fancybox({
            // Options will go here
            toolbar : true,
            animationEffect : "zoom-in-out",
            transitionEffect : "circular",
            thumbs : {
                autoStart   : false,                  // Display thumbnails on opening
                hideOnClose : true,                   // Hide thumbnail grid when closing animation starts
                parentEl    : '.fancybox-container',  // Container is injected into this element
                axis        : 'y'                     // Vertical (y) or horizontal (x) scrolling
            },
            buttons : [
                'slideShow',
                'fullScreen',
                'thumbs',
                // 'share',
                //'download',
                //'zoom',
                'close'
            ],

        });

         /* Init Counterup */
        $('.counter').counterUp({
            delay: 4,
            time: 100
        });

        // carousel to mobile touchable
        $('.carousel').each(function() {
            var el = $(this);
            el.swiperight(function() {
                el.carousel('prev');
            });
            el.swipeleft(function() {
                el.carousel('next');
            });
        });


        // Click me to the my section
        $('.click-to-next').on('click',function() {
            var secHeight = $('#main-slider').height();
            $('html').animate({
                scrollTop: secHeight -61
            }, 900);
        });

        // Slick Slider || Testimonial
        // $('.slider-for').slick({
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     arrows: true,
        //     centerMode: true,
        //     fade: true,
        //     dots: true,
        //     centerPadding: 0,
        //     asNavFor: '.slider-nav'
        // });

        $('.slider-nav').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });

        // Sick Slider || Services
        $('.service-slider-content').slick({
            slidesToShow: 1,
            arrows: false,
            draggable: false,
            speed: 500,
            asNavFor: '.slider-thumb',

        });
        $('.slider-thumb').slick({
            arrows: false,
            asNavFor: '.service-slider-content',
            centerMode: false,
            centerPadding: '15px',
            dots: false,
            infinite: false,
            focusOnSelect: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 3,
                  }
                }
            ]
        });

        $('.service-slider-content-2nd').slick({
            slidesToShow: 1,
            arrows: false,
            draggable: false,
            speed: 500,
            asNavFor: '.slider-thumb-2nd',

        });
        $('.slider-thumb-2nd').slick({
            arrows: false,
            asNavFor: '.service-slider-content-2nd',
            centerMode: false,
            centerPadding: '15px',
            dots: false,
            infinite: false,
            focusOnSelect: true,
            slidesToShow: 6,
            slidesToScroll: 6,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 6,
                  }
                }
            ]
        });


         //Slick Nav

        $('.wpb-mobile-menu').slicknav({
            prependTo: '.navbar-header',
            parentTag: 'margo',
            allowParentLinks: true,
            duplicate: false,
            label: '',
            closedSymbol: '<i class="fa fa-plus"></i>',
            openedSymbol: '<i class="fa fa-minus"></i>',
        });


    }); //$(document).ready



    //Back Top Link

    var offset = 200;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(400);
        } else {
            $('.back-to-top').fadeOut(400);
        }
    });


    $(window).on( 'load', function() {

        $('#loader').fadeOut();

       // Isotope portfolio
        var $container = $('.portfolio-container');

        $container.isotope({
            filter: '*',
            masonry: {
                horizontalOrder: true
            },
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolio-filter a').on('click', function() {
            $('.portfolio-filter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                percentPosition: true,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    }); //Window Load End



    // Google Map
    function initMap() {

        $('.init-gmap').each(function() {

            var element = $(this);

            var uluru = {lat: element.data('lat'), lng: element.data('lng')};

            var map = new google.maps.Map( element.get(0), {
                scrollwheel: true,
                zoom: element.data('zoom'),
                center: uluru
            });


            var marker = new google.maps.Marker({
                position: uluru,
                icon: 'assets/images/marker.png',
                map: map,
                title: 'Khulna 9000, Bangladesh'
            });

        });
    }

    $(window).on( 'load', function() {
        initMap();
    });

})(jQuery);






