/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : pmis

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-05-20 18:33:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for abouts
-- ----------------------------
DROP TABLE IF EXISTS `abouts`;
CREATE TABLE `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of abouts
-- ----------------------------
INSERT INTO `abouts` VALUES ('1', null, '2020-05-02 21:23:25', '2020-05-16 16:35:51', '1', '<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Neft, qaz , kimya sənayesi və mülki tikitinti layihələrinin\r\nartdıqca, sifarişçilər diqqəti risklərin, vaxt və xərclərin idarə olunmasına\r\nyönəldirlər. Şirkətlər sertifikatlaşdırılmış keyfiyyət sistemləri və sənaye təcrübəsinə\r\nəsaslanaraq tam miqyaslı layihə idarə xidmətləri təklif edərək bu tələblərə cavab\r\nverə biləcək  tərəfdaşlar axtarırlar.</span><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:red\"=\"\"> <o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Məqsədimiz, layihənin idarə olunması üçün vahid və\r\nuyğunlaşdırılmış yanaşma qəbul edərək hər bir layihəni vaxtında, nəzərdə\r\ntutulmuş büdcə çərçivəsində və razılaşdırılmış keyfiyyətdə tamamlanmasını təmin\r\netməkdir.</span><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:red\"=\"\"> <o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 6pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Layihələrimiz, iri miqyaslı neft və qaz, mülki və sənaye layihələrində\r\ntəcrübəsi olan aparıcı mütəxəssislər tərəfindən idarə və icra olunur</span><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:#141414;mso-ansi-language:az-latin\"=\"\"> </span><span lang=\"AZ-LATIN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:#212121;mso-ansi-language:az-latin\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AZ-LATIN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;color:red;mso-ansi-language:az-latin\"=\"\"><font face=\"Jura\"> </font></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size: 24px;\"><font face=\"Jura\">FƏALİYYƏT DAİRƏMİZ</font><span style=\"color:red\"><o:p></o:p></span></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Risk və imkanların idarə olunması, Dəyişikliklərin idarə\r\nolunması</span><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Layih</span><span style=\"font-size:14.0pt;font-family:\r\n\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:=\"\" minor-latin;mso-bidi-font-family:arial\"=\"\">ələrin planlaması </span><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\"> və iş qrafiklərinin\r\nhazırlanması; İşlərin gedişatı barədə hesabat və raportların hazırlanması</span><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Resursların idarə olunması, xərclərə və budcəyə nəzarət</span><span style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Tender, </span><span style=\"font-size: 14pt; text-indent: -0.25in;\">Qiymətləndirmə və təkliflərin hazırlanması.</span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Smeta, iş həcmlərinə və müqavilələrə mühəndis nəzarəti<o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 24.7pt; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\r\n\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:=\"\" minor-latin;mso-bidi-font-family:arial;color:red;mso-ansi-language:en\"=\"\"> </span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:16.0pt;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-ansi-language:\r\nEN;mso-bidi-font-weight:bold\">MƏQSƏDLƏRİMİZ<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-ansi-language:\r\nEN\">Mükəmməl nəticələr əldə etmək üçün səylərimizi əsirgəmirik – biz müştərilərimizlə\r\nbirlikdə uğur qazanırıq.<o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:14.0pt;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-ansi-language:\r\nEN\">Müştərilərimizə əla iş təcrübəsi və keyfiyyət təklif etmək üçün bacarıq və\r\nimkanlarımızı daim təkmilləşdiririk.<o:p></o:p></span></p>', '<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#111515;mso-ansi-language:en\"=\"\">As the complexity of oil, gas ,\r\npetrochemical projects increases, owners and operators have intensified their\r\nfocus on managing risks, time and cost. Companies are looking for partners who\r\ncan match these expectations by offering full-scale project management services\r\nbased on certified quality systems and industry experience.</span><span style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#212121\"=\"\"> <o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#141414;mso-ansi-language:en\"=\"\">By adopting a holistic and tailored\r\napproach to project management, our goal is to ensure that every project is\r\ncompleted on time and on budget and to the agreed quality.</span><span style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#212121\"=\"\"> <o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#141414;mso-ansi-language:en\"=\"\">Our projects are managed and executed by\r\nexperienced senior staff well-versed in handling large-scale oil and gas\r\nprojects. Our success is demonstrated by a strong project track record and relies\r\non the methodology of the Project Management model.</span><span style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n12.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;\r\nmso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;color:#141414;\r\nmso-ansi-language:EN\">WE COVER<o:p></o:p></span></b></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:\r\nWingdings;color:#212121\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#141414;mso-ansi-language:en\"=\"\">Risk & opportunity\r\nmanagement, and management of change</span><span style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:\r\nWingdings;color:#212121\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#141414;mso-ansi-language:en\"=\"\">Planning &\r\nscheduling; progress reporting and timekeeping </span><span style=\"font-family:\r\n\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:=\"\" minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:\r\nWingdings;color:#212121\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#141414;mso-ansi-language:en\"=\"\">Resource management, cost\r\ncontrol and budget updates</span><span style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:\r\nWingdings;color:#212121\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#141414;mso-ansi-language:en\"=\"\">Tendering, Evaluation and\r\nAward Recommendation</span><span style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#212121\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN\" style=\"font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:\r\nWingdings;color:#141414;mso-ansi-language:EN\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"times=\"\" new=\"\" roman\";=\"\" color:#141414;mso-ansi-language:en\"=\"\">Quantity Surveyor and Contracts commercial\r\nsupport services<o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 24.7pt; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" \"times=\"\" new=\"\" roman\";color:#141414;mso-ansi-language:en\"=\"\"> </span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n12.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;\r\nmso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;color:#141414;\r\nmso-ansi-language:EN\">OUR VALUES<o:p></o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n12.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;\r\nmso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;color:#141414;\r\nmso-ansi-language:EN\">Impact</span></b><span lang=\"EN\" style=\"font-size:12.0pt;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;color:#141414;mso-ansi-language:EN\"> _\r\nWe have a passion for delivering exceptional results - we succeed together with\r\nour customers.<b><o:p></o:p></b></span></p><p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:Calibri;\r\nmso-hansi-theme-font:minor-latin;color:#141414;mso-ansi-language:EN\">Excellence</span></b><span lang=\"EN\" style=\"font-size:12.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\ncolor:#141414;mso-ansi-language:EN\"> _ We continuously develop our competences\r\nto be able to offer the best expertise and quality to customers.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:Calibri;\r\nmso-hansi-theme-font:minor-latin;color:#141414;mso-ansi-language:EN\">Respect </span></b><span lang=\"EN\" style=\"font-size:12.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\ncolor:#141414;mso-ansi-language:EN\">_ We treat others, co-workers and customers\r\nalike, with respect and equality.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n12.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;\r\nmso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;color:#141414;\r\nmso-ansi-language:EN\">OUR VISION<o:p></o:p></span></b></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:12.0pt;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;color:#141414;mso-ansi-language:EN\">Is\r\nto make sure every Construction Project been executed competitively, up to high\r\nquality standards, within the baseline schedule and approved budget.<b><o:p></o:p></b></span></p>', '<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"mso-bidi-font-size:\r\n11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;color:#376092;=\"\" mso-themecolor:accent1;mso-themeshade:191;mso-style-textfill-fill-color:#376092;=\"\" mso-style-textfill-fill-themecolor:accent1;mso-style-textfill-fill-alpha:100.0%;=\"\" mso-style-textfill-fill-colortransforms:lumm=\"75000;mso-ansi-language:EN\" \"=\"\">Haqq</span></b><b><span lang=\"AZ-LATIN\" style=\"mso-bidi-font-size:\r\n11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;color:#376092;=\"\" mso-themecolor:accent1;mso-themeshade:191;mso-style-textfill-fill-color:#376092;=\"\" mso-style-textfill-fill-themecolor:accent1;mso-style-textfill-fill-alpha:100.0%;=\"\" mso-style-textfill-fill-colortransforms:lumm=\"75000;mso-ansi-language:AZ-LATIN\" \"=\"\">ımızda.<o:p></o:p></span></b></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AZ-LATIN\" style=\"mso-bidi-font-size:\r\n11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;=\"\" mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;mso-ansi-language:=\"\" az-latin\"=\"\"><o:p> </o:p></span></b></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Neft, qaz , kimya sənayesi və mülki tikitinti layihələrinin\r\nartdıqca, sifarişçilər diqqəti risklərin, vaxt və xərclərin idarə olunmasına\r\nyönəldirlər. Şirkətlər sertifikatlaşdırılmış keyfiyyət sistemləri və sənaye təcrübəsinə\r\nəsaslanaraq tam miqyaslı layihə idarə xidmətləri təklif edərək bu tələblərə <span style=\"color:#376092;mso-themecolor:accent1;mso-themeshade:191;mso-style-textfill-fill-color:\r\n#376092;mso-style-textfill-fill-themecolor:accent1;mso-style-textfill-fill-alpha:\r\n100.0%;mso-style-textfill-fill-colortransforms:lumm=75000\">cavab</span> verə\r\nbiləcək  tərəfdaşlar axtarırlar.</span><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:red\"=\"\"> <o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Məqsədimiz, layihənin idarə olunması üçün vahid və\r\nuyğunlaşdırılmış yanaşma qəbul edərək hər bir layihəni vaxtında, nəzərdə\r\ntutulmuş büdcə çərçivəsində və razılaşdırılmış keyfiyyətdə tamamlanmasını təmin\r\netməkdir.</span><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:red\"=\"\"> <o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 6pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\">Layihələrimiz, iri miqyaslı neft və qaz, mülki və sənaye layihələrində\r\ntəcrübəsi olan aparıcı mütəxəssislər tərəfindən idarə və icra olunur</span><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;color:#141414;mso-ansi-language:az-latin\"=\"\"> </span><span lang=\"AZ-LATIN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" color:#212121;mso-ansi-language:az-latin\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AZ-LATIN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;color:red;mso-ansi-language:az-latin\"=\"\"><o:p> </o:p></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:12.0pt;\r\nmso-bidi-font-size:11.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:Arial;mso-ansi-language:EN;mso-bidi-font-weight:bold\">FƏALİYYƏT\r\nDAİRƏMİZ<span style=\"color:red\"><o:p></o:p></span></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"mso-bidi-font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:\r\nWingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Risk və imkanların idarə olunması, Dəyişikliklərin\r\nidarə olunması</span><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"mso-bidi-font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:\r\nWingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Layih</span><span style=\"mso-bidi-font-size:11.0pt;\r\nfont-family:\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:=\"\" minor-latin;mso-bidi-font-family:arial\"=\"\">ələrin planlaması </span><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial;=\"\" mso-ansi-language:en\"=\"\"> və iş qrafiklərinin\r\nhazırlanması; İşlərin gedişatı barədə hesabat və raportların hazırlanması</span><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"mso-bidi-font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:\r\nWingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Resursların idarə olunması, xərclərə və budcəyə nəzarət</span><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"mso-bidi-font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:\r\nWingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Tender, Qiymətləndirmə və təkliflərin hazırlanması.</span><span style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;mso-ascii-theme-font:=\"\" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:arial\"=\"\"><o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:\r\nWingdings;mso-bidi-font-family:Wingdings;mso-ansi-language:EN\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\" calibri\",sans-serif;=\"\" mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:=\"\" arial;mso-ansi-language:en\"=\"\">Smetaç işhəcmlərinə və müqavilələrə mühəndis nəzarəti<o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 24.7pt; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"mso-bidi-font-size:11.0pt;font-family:\r\n\" calibri\",sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:=\"\" minor-latin;mso-bidi-font-family:arial;color:red;mso-ansi-language:en\"=\"\"><o:p> </o:p></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:12.0pt;\r\nmso-bidi-font-size:11.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:Arial;mso-ansi-language:EN;mso-bidi-font-weight:bold\">MƏQSƏDLƏRİMİZ<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:12.0pt;\r\nmso-bidi-font-size:11.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:Arial;mso-ansi-language:EN\">Mükəmməl nəticələr əldə etmək\r\nüçün səylərimizi əsirgəmirik – biz müştərilərimizlə birlikdə uğur qazanırıq.<o:p></o:p></span></p><p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:12.0pt;\r\nmso-bidi-font-size:11.0pt;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:Arial;mso-ansi-language:EN\">Müştərilərimizə əla iş təcrübəsi\r\nvə keyfiyyət təklif etmək üçün bacarıq və imkanlarımızı daim təkmilləşdiririk.<o:p></o:p></span></p>');

-- ----------------------------
-- Table structure for about_us_shorts
-- ----------------------------
DROP TABLE IF EXISTS `about_us_shorts`;
CREATE TABLE `about_us_shorts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of about_us_shorts
-- ----------------------------
INSERT INTO `about_us_shorts` VALUES ('1', null, '2020-05-16 12:31:11', '2020-05-16 18:18:57', '<p class=\"MsoNormal\" style=\"text-align:justify;text-indent:28.35pt\"><span lang=\"AZ-LATIN\" style=\"font-size: 16px; line-height: 107%; font-family: Jura, serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><font color=\"#26476c\">“PMIS”işçi heyəti- Azərbaycanda Müştəri sektorları seçimi üzrə yüksək\r\nkeyfiyyətli raportlar təklif edən, layihələri yüksək səviyyədə idarə edən təcrübəli\r\npeşəkarlardan ibarətdir.<o:p></o:p></font></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"text-align:justify;text-indent:28.35pt\"><font color=\"#26476c\"><font face=\"Jura\"><span lang=\"AZ-LATIN\" style=\"font-size: 16px; line-height: 107%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Bizim heyət yeni və ya hal-hazirda davam edən layihələrdə Tender\r\nproseslərindən , layihənin təhvil verilməsinə kimi, rəhbərlik və Sifarişçi </span><span lang=\"AZ-LATIN\" style=\"font-size: 16px; line-height: 107%;\">Müqaviləsi\r\n</span></font><span lang=\"AZ-LATIN\" style=\"font-size: 16px; line-height: 107%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><font face=\"Jura\">icrasının idarə edilməsi xidmətlərini\r\ntəklif edir.</font><font face=\"Jura, serif\"><o:p></o:p></font></span></font></p>\r\n\r\n<p class=\"MsoNormal\" style=\"text-align:justify;text-indent:28.35pt\"><span lang=\"AZ-LATIN\" style=\"font-size: 16px; line-height: 107%; font-family: Jura, serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><font color=\"#26476c\"> Bizim məqsədimiz Müştərilərimizin layihə\r\nboyunca iqtisadi tərəfdən xərclərin azaldılması ilə bərabər, keyfiyyət\r\nqorunması, səmərəliliyin artırılması və risklərin minimuma endirilərək  Lahiyənin idarə olunmasını təmin etməkdir.</font></span><span lang=\"AZ-LATIN\" style=\"font-size:14.0pt;line-height:107%;font-family:\"Jura\",serif;\r\nmso-bidi-font-family:\"Times New Roman\";mso-ansi-language:AZ-LATIN\"><o:p></o:p></span></p>', '<font face=\"Jura\" style=\"font-size: 16px;\" color=\"#26476c\">PMIS Project Management team comprise a group of dedicated and experienced\r\nproject management professionals delivering high quality schemes across a\r\nselection of Client sectors throughout Azerbaijan.\r\nOur team provide Project Management and Contract Administration services in\r\nrelation to greenfield , brownfield turnkey and civil projects from Strategic\r\nplanning through to project delivery. </font>', '<span style=\"color: rgb(38, 71, 108); font-family: Jura; font-size: 16px;\">PMIS Project Management team comprise a group of dedicated and experienced project management professionals delivering high quality schemes across a selection of Client sectors throughout Azerbaijan. Our team provide Project Management and Contract Administration services in relation to greenfield , brownfield turnkey and civil projects from Strategic planning through to project delivery. </span>');

-- ----------------------------
-- Table structure for backups
-- ----------------------------
DROP TABLE IF EXISTS `backups`;
CREATE TABLE `backups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `backups_name_unique` (`name`),
  UNIQUE KEY `backups_file_name_unique` (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of backups
-- ----------------------------

-- ----------------------------
-- Table structure for benchmarkings
-- ----------------------------
DROP TABLE IF EXISTS `benchmarkings`;
CREATE TABLE `benchmarkings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of benchmarkings
-- ----------------------------
INSERT INTO `benchmarkings` VALUES ('1', null, '2020-05-01 20:23:14', '2020-05-01 20:23:14', '0', '<div class=\"page-subtitle__title\" style=\"box-sizing: inherit; color: rgb(173, 70, 153); font-size: 1.25em; font-family: Montserrat, sans-serif;\"><em style=\"box-sizing: inherit;\">rhi\'s</em> benchmarking database of performance metrics is derived from actual project performance information.</div><div class=\"page-subtitle__text\" style=\"box-sizing: inherit; font-size: 1.125em; padding-bottom: 1.25em; color: rgb(0, 54, 95); line-height: 2.0625rem; font-family: Montserrat, sans-serif;\"><p style=\"box-sizing: inherit; font-size: 1.125rem; line-height: 2.0625rem;\">This allows us to compare detailed elements of project costs, man-hours and schedules with industry norms.</p><p style=\"box-sizing: inherit; font-size: 1.125rem; line-height: 2.0625rem;\">Our benchmarking process is used to provide clients with project cost and schedule assurance and can be applied at each stage gate throughout the project life cycle.</p><h3 style=\"box-sizing: inherit; font-size: 1.9375rem; margin-bottom: 0px; padding-bottom: 0.9375rem; color: rgb(173, 70, 153);\">RHiData</h3><p style=\"box-sizing: inherit; font-size: 1.125rem; line-height: 2.0625rem;\">RHiData is an extensive in-house database and system containing historical and current global data on pricing, manhours and quantities.</p><p style=\"box-sizing: inherit; font-size: 1.125rem; line-height: 2.0625rem;\"><strong style=\"box-sizing: inherit;\">Benefits:</strong></p><ul style=\"box-sizing: inherit; font-size: 1.125rem; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-bottom: 0.9375rem; line-height: 2.0625em; list-style: outside url(\"mysource_files/list-style-image.png\") circle; padding-left: 1.125rem;\"><li style=\"box-sizing: inherit; padding-left: 0.75rem; padding-bottom: 0px;\">Database populated with recent and relevant project data</li><li style=\"box-sizing: inherit; padding-left: 0.75rem; padding-bottom: 0px;\">Useful reference tool for quick response to client enquiries</li><li style=\"box-sizing: inherit; padding-left: 0.75rem; padding-bottom: 0px;\">Source of benchmarking and modelling data</li><li style=\"box-sizing: inherit; padding-left: 0.75rem; padding-bottom: 0px;\">Client assurance that project estimates represent market consistency</li></ul></div>', '', '');

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logo` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES ('1', null, '2020-05-16 12:17:16', '2020-05-16 12:17:16', '3', '', '', '');
INSERT INTO `clients` VALUES ('2', null, '2020-05-16 13:56:41', '2020-05-16 13:56:41', '3', '', '', '');
INSERT INTO `clients` VALUES ('3', null, '2020-05-16 13:56:59', '2020-05-16 13:56:59', '3', '', '', '');
INSERT INTO `clients` VALUES ('4', null, '2020-05-16 13:57:23', '2020-05-16 13:57:23', '3', '', '', '');

-- ----------------------------
-- Table structure for contact_infos
-- ----------------------------
DROP TABLE IF EXISTS `contact_infos`;
CREATE TABLE `contact_infos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contact_infos
-- ----------------------------
INSERT INTO `contact_infos` VALUES ('1', null, '2020-05-01 21:08:52', '2020-05-07 07:41:18', '+994558913281', 'emil9453@gmail.com', 'http://linkedin.com/in/emil-bagirov-45b15b154');

-- ----------------------------
-- Table structure for contact_uses
-- ----------------------------
DROP TABLE IF EXISTS `contact_uses`;
CREATE TABLE `contact_uses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contact_uses
-- ----------------------------
INSERT INTO `contact_uses` VALUES ('1', null, '2020-05-04 19:03:42', '2020-05-07 07:52:49', '14', '', '<div class=\"contact-content\" style=\"color: rgb(127, 127, 127); font-family: Poppins, sans-serif; font-size: 12px;\"><div class=\"contact-content\"><div class=\"comm-title\" style=\"font-size: 24px; font-weight: 700; color: rgb(38, 71, 108); text-transform: uppercase; margin-bottom: 30px;\"><h3 style=\"font-family: \" roboto=\"\" slab\",=\"\" serif;=\"\" font-weight:=\"\" 700;=\"\" color:=\"\" rgb(38,=\"\" 71,=\"\" 108);\"=\"\">CONTACT US</h3></div></div><div class=\"lets-contact\"><ul class=\"contact-list\" style=\"margin: 30px 0px; list-style: none; padding: 0px;\"><li style=\"margin-bottom: 10px;\"><a href=\"http://127.0.0.1:8000/contact_us#\" style=\"color: rgb(91, 91, 91); transition: color 0.2s ease-in-out 0s;\"><span class=\"fa fa-paper-plane-o\" style=\"margin-right: 10px; color: rgb(38, 71, 108); padding: 7px; border-radius: 3px; text-align: center;\"></span>1184 Hyde Ave San Jose, CA 95129 West San Jose. USA</a></li><li style=\"margin-bottom: 10px;\"><a href=\"http://127.0.0.1:8000/contact_us#\" style=\"color: rgb(91, 91, 91); transition: color 0.2s ease-in-out 0s;\"><span class=\"fa fa-envelope-o\" style=\"margin-right: 10px; color: rgb(38, 71, 108); padding: 7px; border-radius: 3px; text-align: center;\"></span>admin@yoursite.com</a></li><li style=\"margin-bottom: 10px;\"><a href=\"http://127.0.0.1:8000/contact_us#\" style=\"color: rgb(91, 91, 91); transition: color 0.2s ease-in-out 0s;\"><span class=\"fa fa-phone\" style=\"margin-right: 10px; color: rgb(38, 71, 108); padding: 7px; border-radius: 3px; text-align: center;\"></span>+880 123 727 525</a></li><li style=\"margin-bottom: 10px;\"><a href=\"http://127.0.0.1:8000/contact_us#\" style=\"color: rgb(91, 91, 91); transition: color 0.2s ease-in-out 0s;\"><span class=\"fa fa-history\" style=\"margin-right: 10px; color: rgb(38, 71, 108); padding: 7px; border-radius: 3px; text-align: center;\"></span>9.00 am to 9.00 pm in a day, 7 days a week</a></li></ul></div></div>', '');

-- ----------------------------
-- Table structure for cost_contr_and_subs
-- ----------------------------
DROP TABLE IF EXISTS `cost_contr_and_subs`;
CREATE TABLE `cost_contr_and_subs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cost_contr_and_subs
-- ----------------------------
INSERT INTO `cost_contr_and_subs` VALUES ('1', null, '2020-05-05 04:28:39', '2020-05-05 04:28:39', '10', '', '<p class=\"MsoNormal\">Project Management.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PMIS Project Management team\r\ncomprise a group of dedicated and experienced project management professionals\r\ndelivering high quality schemes across a selection of Client sectors throughout\r\nAzerbaijan.</span><span style=\"font-size:9.0pt;line-height:107%;font-family:\r\n\"Arial\",sans-serif;color:black\"><br>\r\n<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Our team provide Project Management and Contract\r\nAdministration services in relation to greenfield and brownfield turnkey\r\nprojects from Strategic planning through to project delivery. Our aim is to add\r\nvalue and manage risk by working with Clients with our transactional teams to\r\nprovide a comprehensive “one stop” capability.<o:p></o:p></span></span></p>', '');

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `departments_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES ('1', 'Administration', '[]', '#000', null, '2020-05-01 10:35:09', '2020-05-01 10:35:09');

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) unsigned NOT NULL DEFAULT 1,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `date_hire` date NOT NULL,
  `date_left` date NOT NULL DEFAULT '1990-01-01',
  `salary_cur` decimal(15,3) NOT NULL DEFAULT 0.000,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_email_unique` (`email`),
  KEY `employees_dept_foreign` (`dept`),
  CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('1', 'Super Admin', 'Super Admin', 'Male', '8888888888', '', 'user@example.com', '1', 'Pune', 'Karve nagar, Pune 411030', 'About user / biography', '2020-05-01', '2020-05-01', '2020-05-01', '0.000', null, '2020-05-01 10:35:27', '2020-05-01 10:35:27');

-- ----------------------------
-- Table structure for homes
-- ----------------------------
DROP TABLE IF EXISTS `homes`;
CREATE TABLE `homes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `slogan_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `slogan_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `slogan_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of homes
-- ----------------------------
INSERT INTO `homes` VALUES ('1', null, '2020-05-16 12:23:33', '2020-05-20 12:51:02', '14', '<b style=\"text-align: justify; text-indent: 37.8px;\"><span lang=\"AZ-LATIN\" times=\"\" new=\"\" roman\";mso-ansi-language:az-latin\"=\"\" style=\"font-size: 48px; line-height: 51.36px;\"><font face=\"Jura\">Layihələrin idarə olunması.</font></span></b>', '<b style=\"text-align: justify; text-indent: 37.8px;\"><span lang=\"AZ-LATIN\" times=\"\" new=\"\" roman\";mso-ansi-language:az-latin\"=\"\" style=\"font-size: 48px; line-height: 51.36px;\"><font face=\"Jura\">Project Management.</font></span></b><br>', '<b style=\"text-align: justify; text-indent: 37.8px;\"><span lang=\"AZ-LATIN\" times=\"\" new=\"\" roman\";mso-ansi-language:az-latin\"=\"\" style=\"font-size: 48px; line-height: 51.36px;\"><font face=\"Jura\">Project Management.</font></span></b>', 'Məqsədimiz  Layihələrinizin bütün mərhələlərində Sizin vaxtınıza və pulunuza qənaəti təmin etməkdir.', 'Helping you from  take-off to re-measurement due to saving you time and money at every stage of Project', 'Helping you from  take-off to re-measurement due to saving you time and money at every stage of Project');

-- ----------------------------
-- Table structure for la_configs
-- ----------------------------
DROP TABLE IF EXISTS `la_configs`;
CREATE TABLE `la_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of la_configs
-- ----------------------------
INSERT INTO `la_configs` VALUES ('1', 'sitename', '', 'PMIS', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('2', 'sitename_part1', '', 'PMIS', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('3', 'sitename_part2', '', '', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('4', 'sitename_short', '', '', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('5', 'site_description', '', '', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('6', 'sidebar_search', '', '0', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('7', 'show_messages', '', '0', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('8', 'show_notifications', '', '0', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('9', 'show_tasks', '', '0', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('10', 'show_rightsidebar', '', '0', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('11', 'skin', '', 'skin-white', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('12', 'layout', '', 'layout-top-nav', '2020-05-01 10:35:10', '2020-05-01 20:54:21');
INSERT INTO `la_configs` VALUES ('13', 'default_email', '', 'info@pmis.az', '2020-05-01 10:35:10', '2020-05-01 20:54:21');

-- ----------------------------
-- Table structure for la_menus
-- ----------------------------
DROP TABLE IF EXISTS `la_menus`;
CREATE TABLE `la_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) unsigned NOT NULL DEFAULT 0,
  `hierarchy` int(10) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of la_menus
-- ----------------------------
INSERT INTO `la_menus` VALUES ('1', 'WHAT WE DO', '#', 'fa-group', 'custom', '0', '4', '2020-05-01 10:35:09', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('3', 'Uploads', 'uploads', 'fa-files-o', 'module', '0', '2', '2020-05-01 10:35:09', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('12', 'Prop_sp_and_cost_ests', 'prop_sp_and_cost_ests', 'fa fa-cube', 'module', '1', '2', '2020-05-01 11:36:20', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('13', 'Planning_and_scheds', 'planning_and_scheds', 'fa fa-cube', 'module', '1', '3', '2020-05-01 13:06:54', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('14', 'Quality_s_and_con_ads', 'quality_s_and_con_ads', 'fa fa-cube', 'module', '1', '4', '2020-05-01 13:16:45', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('15', 'Cost_contr_and_subs', 'cost_contr_and_subs', 'fa fa-cube', 'module', '1', '5', '2020-05-01 13:19:59', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('16', 'Timekeeperings', 'timekeeperings', 'fa fa-cube', 'module', '1', '6', '2020-05-01 13:23:43', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('17', 'Benchmarkings', 'benchmarkings', 'fa fa-cube', 'module', '1', '7', '2020-05-01 13:26:41', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('18', 'Reporting_and_autos', 'reporting_and_autos', 'fa fa-cube', 'module', '1', '8', '2020-05-01 13:30:04', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('19', 'Abouts', 'abouts', 'fa fa-cube', 'module', '24', '2', '2020-05-01 13:34:10', '2020-05-01 21:01:12');
INSERT INTO `la_menus` VALUES ('20', 'Team_members', 'team_members', 'fa fa-cube', 'module', '24', '1', '2020-05-01 20:32:51', '2020-05-01 21:00:38');
INSERT INTO `la_menus` VALUES ('21', 'Clients', 'clients', 'fa fa-cube', 'module', '24', '3', '2020-05-01 20:48:15', '2020-05-01 21:01:12');
INSERT INTO `la_menus` VALUES ('24', 'WHO WE ARE', '#', 'fa-group', 'custom', '0', '3', '2020-05-01 10:35:09', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('25', 'Contact_infos', 'contact_infos', 'fa fa-cube', 'module', '0', '1', '2020-05-01 21:06:28', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('26', 'Project_serv_sols', 'project_serv_sols', 'fa fa-cube', 'module', '1', '1', '2020-05-03 11:16:55', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('27', 'Our_team_images', 'our_team_images', 'fa fa-cube', 'module', '0', '0', '2020-05-03 13:23:12', '2020-05-03 13:23:12');
INSERT INTO `la_menus` VALUES ('29', 'Contact_uses', 'contact_uses', 'fa fa-cube', 'module', '0', '0', '2020-05-04 18:58:09', '2020-05-04 18:58:09');
INSERT INTO `la_menus` VALUES ('30', 'Homes', 'homes', 'fa fa-cube', 'module', '33', '0', '2020-05-16 11:29:07', '2020-05-16 11:29:07');
INSERT INTO `la_menus` VALUES ('31', 'About_us_shorts', 'about_us_shorts', 'fa fa-cube', 'module', '33', '0', '2020-05-16 11:31:52', '2020-05-16 11:31:52');
INSERT INTO `la_menus` VALUES ('33', 'Home', '#', 'fa-group', 'custom', '0', '3', '2020-05-01 10:35:09', '2020-05-03 11:17:12');
INSERT INTO `la_menus` VALUES ('35', 'Vacancies', 'vacancies', 'fa fa-cube', 'module', '0', '0', '2020-05-19 07:08:37', '2020-05-19 07:08:37');
INSERT INTO `la_menus` VALUES ('36', 'Vacancy_images', 'vacancy_images', 'fa fa-cube', 'module', '0', '0', '2020-05-19 08:27:00', '2020-05-19 08:27:00');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_05_26_050000_create_modules_table', '1');
INSERT INTO `migrations` VALUES ('2014_05_26_055000_create_module_field_types_table', '1');
INSERT INTO `migrations` VALUES ('2014_05_26_060000_create_module_fields_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2014_12_01_000000_create_uploads_table', '1');
INSERT INTO `migrations` VALUES ('2016_05_26_064006_create_departments_table', '1');
INSERT INTO `migrations` VALUES ('2016_05_26_064007_create_employees_table', '1');
INSERT INTO `migrations` VALUES ('2016_05_26_064446_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_05_115343_create_role_user_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_06_140637_create_organizations_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_07_134058_create_backups_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_07_134058_create_menus_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_10_163337_create_permissions_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_10_163520_create_permission_role_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_22_105958_role_module_fields_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_22_110008_role_module_table', '1');
INSERT INTO `migrations` VALUES ('2016_10_06_115413_create_la_configs_table', '1');
INSERT INTO `migrations` VALUES ('2020_04_30_205312_create_about_uses_table', '1');
INSERT INTO `migrations` VALUES ('2020_04_30_213916_create_abouts_table', '1');
INSERT INTO `migrations` VALUES ('2020_05_01_111936_create_prop_sp_and_cost_ests_table', '1');

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', '1', '2020-05-01 10:35:05', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('2', 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', '1', '2020-05-01 10:35:05', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('3', 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', '1', '2020-05-01 10:35:05', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('4', 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', '1', '2020-05-01 10:35:05', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('5', 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', '1', '2020-05-01 10:35:06', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('6', 'Organizations', 'Organizations', 'organizations', 'name', 'Organization', 'OrganizationsController', 'fa-university', '1', '2020-05-01 10:35:07', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('7', 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', '1', '2020-05-01 10:35:07', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('8', 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', '1', '2020-05-01 10:35:07', '2020-05-01 10:35:10');
INSERT INTO `modules` VALUES ('15', 'Prop_sp_and_cost_ests', 'Prop_sp_and_cost_ests', 'prop_sp_and_cost_ests', 'text_az', 'Prop_sp_and_cost_est', 'Prop_sp_and_cost_estsController', 'fa-cube', '1', '2020-05-01 11:35:37', '2020-05-01 11:37:18');
INSERT INTO `modules` VALUES ('16', 'Planning_and_scheds', 'Planning_and_scheds', 'planning_and_scheds', 'image', 'Planning_and_sched', 'Planning_and_schedsController', 'fa-cube', '1', '2020-05-01 13:04:48', '2020-05-01 13:06:54');
INSERT INTO `modules` VALUES ('17', 'Quality_s_and_con_ads', 'Quality_s_and_con_ads', 'quality_s_and_con_ads', 'image', 'Quality_s_and_con_ad', 'Quality_s_and_con_adsController', 'fa-cube', '1', '2020-05-01 13:13:43', '2020-05-01 13:16:45');
INSERT INTO `modules` VALUES ('18', 'Cost_contr_and_subs', 'Cost_contr_and_subs', 'cost_contr_and_subs', 'image', 'Cost_contr_and_sub', 'Cost_contr_and_subsController', 'fa-cube', '1', '2020-05-01 13:17:51', '2020-05-01 13:19:59');
INSERT INTO `modules` VALUES ('19', 'Timekeeperings', 'Timekeeperings', 'timekeeperings', 'image', 'Timekeepering', 'TimekeeperingsController', 'fa-cube', '1', '2020-05-01 13:21:35', '2020-05-01 13:23:43');
INSERT INTO `modules` VALUES ('20', 'Benchmarkings', 'Benchmarkings', 'benchmarkings', 'image', 'Benchmarking', 'BenchmarkingsController', 'fa-cube', '1', '2020-05-01 13:24:15', '2020-05-01 13:26:41');
INSERT INTO `modules` VALUES ('21', 'Reporting_and_autos', 'Reporting_and_autos', 'reporting_and_autos', 'image', 'Reporting_and_auto', 'Reporting_and_autosController', 'fa-cube', '1', '2020-05-01 13:27:36', '2020-05-01 13:30:04');
INSERT INTO `modules` VALUES ('22', 'Abouts', 'Abouts', 'abouts', 'image', 'About', 'AboutsController', 'fa-cube', '1', '2020-05-01 13:33:36', '2020-05-01 13:34:10');
INSERT INTO `modules` VALUES ('23', 'Team_members', 'Team_members', 'team_members', 'name', 'Team_member', 'Team_membersController', 'fa-cube', '1', '2020-05-01 20:26:20', '2020-05-01 20:32:51');
INSERT INTO `modules` VALUES ('24', 'Clients', 'Clients', 'clients', 'logo', 'Client', 'ClientsController', 'fa-cube', '1', '2020-05-01 20:42:49', '2020-05-01 20:48:15');
INSERT INTO `modules` VALUES ('25', 'Contact_infos', 'Contact_infos', 'contact_infos', 'phone', 'Contact_info', 'Contact_infosController', 'fa-cube', '1', '2020-05-01 21:05:05', '2020-05-01 21:06:28');
INSERT INTO `modules` VALUES ('26', 'Project_serv_sols', 'Project_serv_sols', 'project_serv_sols', 'image', 'Project_serv_sol', 'Project_serv_solsController', 'fa-cube', '1', '2020-05-03 11:14:39', '2020-05-03 11:16:55');
INSERT INTO `modules` VALUES ('27', 'Our_team_images', 'Our_team_images', 'our_team_images', 'image', 'Our_team_image', 'Our_team_imagesController', 'fa-cube', '1', '2020-05-03 13:22:46', '2020-05-03 13:23:12');
INSERT INTO `modules` VALUES ('31', 'Contact_uses', 'Contact_uses', 'contact_uses', 'image', 'Contact_us', 'Contact_usesController', 'fa-cube', '1', '2020-05-04 18:57:35', '2020-05-04 18:58:09');
INSERT INTO `modules` VALUES ('32', 'Homes', 'Homes', 'homes', 'image', 'Home', 'HomesController', 'fa-cube', '1', '2020-05-16 11:20:22', '2020-05-16 11:29:07');
INSERT INTO `modules` VALUES ('33', 'About_us_shorts', 'About_us_shorts', 'about_us_shorts', 'text_az', 'About_us_short', 'About_us_shortsController', 'fa-cube', '1', '2020-05-16 11:30:27', '2020-05-16 11:31:52');
INSERT INTO `modules` VALUES ('36', 'Vacancies', 'Vacancies', 'vacancies', 'title', 'Vacancy', 'VacanciesController', 'fa-cube', '1', '2020-05-19 07:07:56', '2020-05-19 07:08:37');
INSERT INTO `modules` VALUES ('37', 'Vacancy_images', 'Vacancy_images', 'vacancy_images', 'image', 'Vacancy_image', 'Vacancy_imagesController', 'fa-cube', '1', '2020-05-19 08:25:52', '2020-05-19 08:27:00');

-- ----------------------------
-- Table structure for module_fields
-- ----------------------------
DROP TABLE IF EXISTS `module_fields`;
CREATE TABLE `module_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) unsigned NOT NULL,
  `field_type` int(10) unsigned NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT 0,
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) unsigned NOT NULL DEFAULT 0,
  `maxlength` int(10) unsigned NOT NULL DEFAULT 0,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_fields_module_foreign` (`module`),
  KEY `module_fields_field_type_foreign` (`field_type`),
  CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of module_fields
-- ----------------------------
INSERT INTO `module_fields` VALUES ('1', 'name', 'Name', '1', '16', '0', '', '5', '250', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('2', 'context_id', 'Context', '1', '13', '0', '0', '0', '0', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('3', 'email', 'Email', '1', '8', '1', '', '0', '250', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('4', 'password', 'Password', '1', '17', '0', '', '6', '250', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('5', 'type', 'User Type', '1', '7', '0', 'Employee', '0', '0', '0', '[\"Employee\",\"Client\"]', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('6', 'name', 'Name', '2', '16', '0', '', '5', '250', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('7', 'path', 'Path', '2', '19', '0', '', '0', '250', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('8', 'extension', 'Extension', '2', '19', '0', '', '0', '20', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('9', 'caption', 'Caption', '2', '19', '0', '', '0', '250', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('10', 'user_id', 'Owner', '2', '7', '0', '1', '0', '0', '0', '@users', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('11', 'hash', 'Hash', '2', '19', '0', '', '0', '250', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('12', 'public', 'Is Public', '2', '2', '0', '0', '0', '0', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('13', 'name', 'Name', '3', '16', '1', '', '1', '250', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('14', 'tags', 'Tags', '3', '20', '0', '[]', '0', '0', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('15', 'color', 'Color', '3', '19', '0', '', '0', '50', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('16', 'name', 'Name', '4', '16', '0', '', '5', '250', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('17', 'designation', 'Designation', '4', '19', '0', '', '0', '50', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('18', 'gender', 'Gender', '4', '18', '0', 'Male', '0', '0', '1', '[\"Male\",\"Female\"]', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('19', 'mobile', 'Mobile', '4', '14', '0', '', '10', '20', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('20', 'mobile2', 'Alternative Mobile', '4', '14', '0', '', '10', '20', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('21', 'email', 'Email', '4', '8', '1', '', '5', '250', '1', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('22', 'dept', 'Department', '4', '7', '0', '0', '0', '0', '1', '@departments', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('23', 'city', 'City', '4', '19', '0', '', '0', '50', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('24', 'address', 'Address', '4', '1', '0', '', '0', '1000', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('25', 'about', 'About', '4', '19', '0', '', '0', '0', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('26', 'date_birth', 'Date of Birth', '4', '4', '0', '1990-01-01', '0', '0', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('27', 'date_hire', 'Hiring Date', '4', '4', '0', 'date(\'Y-m-d\')', '0', '0', '0', '', '0', '2020-05-01 10:35:05', '2020-05-01 10:35:05');
INSERT INTO `module_fields` VALUES ('28', 'date_left', 'Resignation Date', '4', '4', '0', '1990-01-01', '0', '0', '0', '', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('29', 'salary_cur', 'Current Salary', '4', '6', '0', '0.0', '0', '2', '0', '', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('30', 'name', 'Name', '5', '16', '1', '', '1', '250', '1', '', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('31', 'display_name', 'Display Name', '5', '19', '0', '', '0', '250', '1', '', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('32', 'description', 'Description', '5', '21', '0', '', '0', '1000', '0', '', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('33', 'parent', 'Parent Role', '5', '7', '0', '1', '0', '0', '0', '@roles', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('34', 'dept', 'Department', '5', '7', '0', '1', '0', '0', '0', '@departments', '0', '2020-05-01 10:35:06', '2020-05-01 10:35:06');
INSERT INTO `module_fields` VALUES ('35', 'name', 'Name', '6', '16', '1', '', '5', '250', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('36', 'email', 'Email', '6', '8', '1', '', '0', '250', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('37', 'phone', 'Phone', '6', '14', '0', '', '0', '20', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('38', 'website', 'Website', '6', '23', '0', 'http://', '0', '250', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('39', 'assigned_to', 'Assigned to', '6', '7', '0', '0', '0', '0', '0', '@employees', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('40', 'connect_since', 'Connected Since', '6', '4', '0', 'date(\'Y-m-d\')', '0', '0', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('41', 'address', 'Address', '6', '1', '0', '', '0', '1000', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('42', 'city', 'City', '6', '19', '0', '', '0', '250', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('43', 'description', 'Description', '6', '21', '0', '', '0', '1000', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('44', 'profile_image', 'Profile Image', '6', '12', '0', '', '0', '250', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('45', 'profile', 'Company Profile', '6', '9', '0', '', '0', '250', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('46', 'name', 'Name', '7', '16', '1', '', '0', '250', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('47', 'file_name', 'File Name', '7', '19', '1', '', '0', '250', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('48', 'backup_size', 'File Size', '7', '19', '0', '0', '0', '10', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('49', 'name', 'Name', '8', '16', '1', '', '1', '250', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('50', 'display_name', 'Display Name', '8', '19', '0', '', '0', '250', '1', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('51', 'description', 'Description', '8', '21', '0', '', '0', '1000', '0', '', '0', '2020-05-01 10:35:07', '2020-05-01 10:35:07');
INSERT INTO `module_fields` VALUES ('68', 'image', 'image', '15', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 11:37:49', '2020-05-01 11:37:49');
INSERT INTO `module_fields` VALUES ('69', 'text_az', 'AZ', '15', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 11:38:25', '2020-05-01 11:38:25');
INSERT INTO `module_fields` VALUES ('70', 'text_en', 'EN', '15', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 11:38:48', '2020-05-01 11:38:48');
INSERT INTO `module_fields` VALUES ('71', 'text_ru', 'RU', '15', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 11:39:08', '2020-05-01 11:39:08');
INSERT INTO `module_fields` VALUES ('72', 'image', 'image', '16', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:06:42', '2020-05-01 13:06:42');
INSERT INTO `module_fields` VALUES ('73', 'text_az', 'AZ', '16', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:08:56', '2020-05-01 13:08:56');
INSERT INTO `module_fields` VALUES ('74', 'text_en', 'EN', '16', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:09:34', '2020-05-01 13:09:34');
INSERT INTO `module_fields` VALUES ('75', 'text_ru', 'RU', '16', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:09:59', '2020-05-01 13:09:59');
INSERT INTO `module_fields` VALUES ('76', 'image', 'image', '17', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:14:38', '2020-05-01 13:14:38');
INSERT INTO `module_fields` VALUES ('77', 'text_az', 'AZ', '17', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:15:15', '2020-05-01 13:15:15');
INSERT INTO `module_fields` VALUES ('78', 'text_en', 'EN', '17', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:15:33', '2020-05-01 13:15:33');
INSERT INTO `module_fields` VALUES ('79', 'text_ru', 'RU', '17', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:15:56', '2020-05-01 13:15:56');
INSERT INTO `module_fields` VALUES ('80', 'image', 'image', '18', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:18:15', '2020-05-01 13:18:15');
INSERT INTO `module_fields` VALUES ('81', 'text_az', 'AZ', '18', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:18:43', '2020-05-01 13:18:43');
INSERT INTO `module_fields` VALUES ('82', 'text_en', 'EN', '18', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:19:13', '2020-05-01 13:19:13');
INSERT INTO `module_fields` VALUES ('83', 'text_ru', 'RU', '18', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:19:35', '2020-05-01 13:19:35');
INSERT INTO `module_fields` VALUES ('84', 'image', 'image', '19', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:22:34', '2020-05-01 13:22:34');
INSERT INTO `module_fields` VALUES ('85', 'text_az', 'AZ', '19', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:22:50', '2020-05-01 13:22:50');
INSERT INTO `module_fields` VALUES ('86', 'text_en', 'EN', '19', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:23:08', '2020-05-01 13:23:08');
INSERT INTO `module_fields` VALUES ('87', 'text_ru', 'RU', '19', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:23:34', '2020-05-01 13:23:34');
INSERT INTO `module_fields` VALUES ('88', 'image', 'image', '20', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:24:48', '2020-05-01 13:24:48');
INSERT INTO `module_fields` VALUES ('89', 'text_az', 'AZ', '20', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:25:18', '2020-05-01 13:25:18');
INSERT INTO `module_fields` VALUES ('90', 'text_en', 'EN', '20', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:25:32', '2020-05-01 13:25:32');
INSERT INTO `module_fields` VALUES ('91', 'text_ru', 'RU', '20', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:26:25', '2020-05-01 13:26:25');
INSERT INTO `module_fields` VALUES ('92', 'image', 'image', '21', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:28:10', '2020-05-01 13:28:10');
INSERT INTO `module_fields` VALUES ('93', 'text_az', 'AZ', '21', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:28:26', '2020-05-01 13:28:26');
INSERT INTO `module_fields` VALUES ('94', 'text_en', 'EN', '21', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:28:40', '2020-05-01 13:28:40');
INSERT INTO `module_fields` VALUES ('95', 'text_ru', 'RU', '21', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:29:03', '2020-05-01 13:29:03');
INSERT INTO `module_fields` VALUES ('96', 'image', 'image', '22', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:34:01', '2020-05-01 13:34:01');
INSERT INTO `module_fields` VALUES ('97', 'text_az', 'AZ', '22', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:34:31', '2020-05-01 13:34:31');
INSERT INTO `module_fields` VALUES ('98', 'text_en', 'EN', '22', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:34:50', '2020-05-01 13:34:50');
INSERT INTO `module_fields` VALUES ('99', 'text_ru', 'RU', '22', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 13:35:04', '2020-05-01 13:35:04');
INSERT INTO `module_fields` VALUES ('100', 'name_az', 'name az', '23', '22', '0', '', '0', '256', '0', '', '1', '2020-05-01 20:28:25', '2020-05-20 13:11:20');
INSERT INTO `module_fields` VALUES ('101', 'profile_image', 'profile image', '23', '12', '0', '', '0', '0', '0', '', '4', '2020-05-01 20:29:31', '2020-05-01 20:29:31');
INSERT INTO `module_fields` VALUES ('103', 'mail', 'mail', '23', '8', '0', '', '0', '256', '0', '', '5', '2020-05-01 20:30:29', '2020-05-01 20:30:29');
INSERT INTO `module_fields` VALUES ('104', 'linkedIn', 'linkedIn', '23', '23', '0', '', '0', '256', '0', '', '6', '2020-05-01 20:31:11', '2020-05-01 20:31:11');
INSERT INTO `module_fields` VALUES ('105', 'logo', 'logo', '24', '12', '0', '', '0', '0', '0', '', '0', '2020-05-01 20:43:20', '2020-05-01 20:43:20');
INSERT INTO `module_fields` VALUES ('106', 'text_az', 'AZ', '24', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 20:45:58', '2020-05-01 20:45:58');
INSERT INTO `module_fields` VALUES ('107', 'text_en', 'EN', '24', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 20:46:17', '2020-05-01 20:46:17');
INSERT INTO `module_fields` VALUES ('108', 'text_ru', 'RU', '24', '21', '0', '', '0', '0', '0', '', '0', '2020-05-01 20:48:02', '2020-05-01 20:48:02');
INSERT INTO `module_fields` VALUES ('109', 'position_az', 'position az', '23', '22', '0', '', '0', '256', '0', '', '7', '2020-05-01 20:50:18', '2020-05-01 20:50:18');
INSERT INTO `module_fields` VALUES ('110', 'position_en', 'position en', '23', '22', '0', '', '0', '256', '0', '', '8', '2020-05-01 20:50:56', '2020-05-01 20:50:56');
INSERT INTO `module_fields` VALUES ('111', 'position_ru', 'postion ru', '23', '21', '0', '', '0', '0', '0', '', '9', '2020-05-01 20:51:17', '2020-05-01 20:51:17');
INSERT INTO `module_fields` VALUES ('112', 'phone', 'phone', '25', '14', '0', '', '0', '20', '0', '', '0', '2020-05-01 21:05:31', '2020-05-01 21:05:31');
INSERT INTO `module_fields` VALUES ('113', 'email', 'email', '25', '8', '0', '', '0', '256', '0', '', '0', '2020-05-01 21:06:11', '2020-05-01 21:06:11');
INSERT INTO `module_fields` VALUES ('114', 'image', 'image', '26', '12', '0', '', '0', '0', '0', '', '0', '2020-05-03 11:15:19', '2020-05-03 11:15:19');
INSERT INTO `module_fields` VALUES ('115', 'text_az', 'AZ', '26', '21', '0', '', '0', '0', '0', '', '0', '2020-05-03 11:15:43', '2020-05-03 11:15:43');
INSERT INTO `module_fields` VALUES ('116', 'text_en', 'EN', '26', '21', '0', '', '0', '0', '0', '', '0', '2020-05-03 11:16:07', '2020-05-03 11:16:07');
INSERT INTO `module_fields` VALUES ('117', 'text_ru', 'RU', '26', '21', '0', '', '0', '0', '0', '', '0', '2020-05-03 11:16:39', '2020-05-03 11:16:39');
INSERT INTO `module_fields` VALUES ('118', 'member_type', 'member type', '23', '7', '0', '', '0', '256', '0', '[\"OUR LEADERSHIP\",\"OUR PEOPLE\",\"OUR EXPERT WITNESSES\"]', '10', '2020-05-03 12:55:40', '2020-05-03 12:58:57');
INSERT INTO `module_fields` VALUES ('120', 'image', 'image', '27', '12', '0', '', '0', '0', '0', '', '0', '2020-05-03 13:23:03', '2020-05-03 13:23:03');
INSERT INTO `module_fields` VALUES ('127', 'image', 'image', '31', '12', '0', '', '0', '0', '0', '', '0', '2020-05-04 18:57:56', '2020-05-04 18:57:56');
INSERT INTO `module_fields` VALUES ('128', 'text_az', 'AZ', '31', '21', '0', '', '0', '0', '0', '', '0', '2020-05-04 18:58:41', '2020-05-04 18:58:41');
INSERT INTO `module_fields` VALUES ('129', 'text_en', 'EN', '31', '21', '0', '', '0', '0', '0', '', '0', '2020-05-04 18:58:58', '2020-05-04 19:03:06');
INSERT INTO `module_fields` VALUES ('130', 'text_ru', 'RU', '31', '21', '0', '', '0', '0', '0', '', '0', '2020-05-04 18:59:28', '2020-05-04 18:59:28');
INSERT INTO `module_fields` VALUES ('131', 'linkedin', 'linkedin', '25', '23', '0', '', '0', '256', '0', '', '0', '2020-05-07 07:39:10', '2020-05-07 07:39:10');
INSERT INTO `module_fields` VALUES ('132', 'leadership_image', 'leadership image', '27', '12', '0', '', '0', '0', '0', '', '0', '2020-05-07 09:06:46', '2020-05-07 09:06:46');
INSERT INTO `module_fields` VALUES ('133', 'people_image', 'people image', '27', '12', '0', '', '0', '0', '0', '', '0', '2020-05-07 09:07:17', '2020-05-07 09:07:17');
INSERT INTO `module_fields` VALUES ('134', 'expert_image', 'expert image', '27', '12', '0', '', '0', '0', '0', '', '0', '2020-05-07 09:08:16', '2020-05-07 09:08:16');
INSERT INTO `module_fields` VALUES ('135', 'image', 'image', '32', '12', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:20:59', '2020-05-16 11:20:59');
INSERT INTO `module_fields` VALUES ('136', 'text_az', 'AZ', '32', '21', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:23:41', '2020-05-16 11:23:41');
INSERT INTO `module_fields` VALUES ('137', 'text_en', 'EN', '32', '21', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:24:00', '2020-05-16 11:24:00');
INSERT INTO `module_fields` VALUES ('138', 'text_ru', 'RU', '32', '21', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:24:18', '2020-05-16 11:24:18');
INSERT INTO `module_fields` VALUES ('142', 'text_az', 'AZ', '33', '21', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:30:48', '2020-05-16 11:30:48');
INSERT INTO `module_fields` VALUES ('143', 'text_en', 'EN', '33', '21', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:31:09', '2020-05-16 11:31:09');
INSERT INTO `module_fields` VALUES ('144', 'text_ru', 'RU', '33', '21', '0', '', '0', '0', '0', '', '0', '2020-05-16 11:31:24', '2020-05-16 11:31:24');
INSERT INTO `module_fields` VALUES ('150', 'title_az', 'title az', '36', '22', '0', '', '0', '256', '0', '', '1', '2020-05-19 07:08:27', '2020-05-19 07:14:19');
INSERT INTO `module_fields` VALUES ('151', 'title_en', 'title en', '36', '22', '0', '', '0', '256', '0', '', '2', '2020-05-19 07:14:36', '2020-05-19 07:14:36');
INSERT INTO `module_fields` VALUES ('152', 'title_ru', 'title ru', '36', '22', '0', '', '0', '256', '0', '', '3', '2020-05-19 07:14:55', '2020-05-19 07:14:55');
INSERT INTO `module_fields` VALUES ('153', 'location_az', 'location az', '36', '22', '0', '', '0', '256', '0', '', '4', '2020-05-19 07:15:27', '2020-05-19 07:15:27');
INSERT INTO `module_fields` VALUES ('154', 'location_en', 'location en ', '36', '22', '0', '', '0', '256', '0', '', '5', '2020-05-19 07:16:09', '2020-05-19 07:16:09');
INSERT INTO `module_fields` VALUES ('155', 'location_ru', 'location ru', '36', '22', '0', '', '0', '256', '0', '', '6', '2020-05-19 07:16:43', '2020-05-19 07:16:43');
INSERT INTO `module_fields` VALUES ('156', 'position_az', 'position az', '36', '22', '0', '', '0', '256', '0', '', '7', '2020-05-19 07:18:51', '2020-05-19 07:19:17');
INSERT INTO `module_fields` VALUES ('157', 'position_en', 'position en', '36', '22', '0', '', '0', '256', '0', '', '8', '2020-05-19 07:19:50', '2020-05-19 07:19:50');
INSERT INTO `module_fields` VALUES ('158', 'position_ru', 'position ru', '36', '22', '0', '', '0', '256', '0', '', '9', '2020-05-19 07:20:16', '2020-05-19 07:20:16');
INSERT INTO `module_fields` VALUES ('159', 'age', 'age', '36', '22', '0', '', '0', '256', '0', '', '10', '2020-05-19 07:21:25', '2020-05-19 07:21:25');
INSERT INTO `module_fields` VALUES ('160', 'education_az', 'education az', '36', '22', '0', '', '0', '256', '0', '', '11', '2020-05-19 07:22:29', '2020-05-19 07:22:29');
INSERT INTO `module_fields` VALUES ('161', 'education_en', 'education en', '36', '22', '0', '', '0', '256', '0', '', '12', '2020-05-19 07:23:19', '2020-05-19 07:23:19');
INSERT INTO `module_fields` VALUES ('162', 'education_ru', 'education ru', '36', '22', '0', '', '0', '256', '0', '', '13', '2020-05-19 07:23:46', '2020-05-19 07:23:46');
INSERT INTO `module_fields` VALUES ('163', 'experience_az', 'experience az', '36', '22', '0', '', '0', '256', '0', '', '14', '2020-05-19 07:25:41', '2020-05-19 07:25:41');
INSERT INTO `module_fields` VALUES ('164', 'experience_en', 'experience en', '36', '22', '0', '', '0', '256', '0', '', '15', '2020-05-19 07:26:02', '2020-05-19 07:26:02');
INSERT INTO `module_fields` VALUES ('165', 'experience_ru', 'experience ru', '36', '22', '0', '', '0', '256', '0', '', '16', '2020-05-19 07:26:24', '2020-05-19 07:26:24');
INSERT INTO `module_fields` VALUES ('166', 'salary', 'salary', '36', '22', '0', '', '0', '256', '0', '', '17', '2020-05-19 07:27:02', '2020-05-19 07:27:02');
INSERT INTO `module_fields` VALUES ('167', 'schedule_az', 'schedule az', '36', '22', '0', '', '0', '256', '0', '', '18', '2020-05-19 07:28:10', '2020-05-19 07:28:24');
INSERT INTO `module_fields` VALUES ('168', 'schedule_en', 'schedule en', '36', '22', '0', '', '0', '256', '0', '', '19', '2020-05-19 07:28:41', '2020-05-19 07:28:41');
INSERT INTO `module_fields` VALUES ('169', 'schedule_ru', 'schedule ru', '36', '22', '0', '', '0', '256', '0', '', '20', '2020-05-19 07:29:03', '2020-05-19 07:29:03');
INSERT INTO `module_fields` VALUES ('170', 'published_on', 'published on', '36', '22', '0', '', '0', '256', '0', '', '21', '2020-05-19 08:08:26', '2020-05-19 08:08:26');
INSERT INTO `module_fields` VALUES ('171', 'expired_on', 'expired on', '36', '22', '0', '', '0', '256', '0', '', '22', '2020-05-19 08:08:52', '2020-05-19 08:08:52');
INSERT INTO `module_fields` VALUES ('172', 'meal', 'meal', '36', '7', '0', '', '0', '256', '0', '[\"yes\",\"no\"]', '23', '2020-05-19 08:09:45', '2020-05-19 08:20:39');
INSERT INTO `module_fields` VALUES ('173', 'description_az', 'description az', '36', '21', '0', '', '0', '0', '0', '', '25', '2020-05-19 08:12:10', '2020-05-19 08:12:10');
INSERT INTO `module_fields` VALUES ('174', 'description_en', 'description en', '36', '21', '0', '', '0', '0', '0', '', '26', '2020-05-19 08:12:36', '2020-05-19 08:12:36');
INSERT INTO `module_fields` VALUES ('175', 'description_ru', 'description ru', '36', '21', '0', '', '0', '0', '0', '', '27', '2020-05-19 08:13:18', '2020-05-19 08:13:18');
INSERT INTO `module_fields` VALUES ('176', 'requirements_az', 'requirements az', '36', '21', '0', '', '0', '0', '0', '', '28', '2020-05-19 08:14:27', '2020-05-19 08:14:27');
INSERT INTO `module_fields` VALUES ('177', 'requirements_en', 'requirements en', '36', '21', '0', '', '0', '0', '0', '', '29', '2020-05-19 08:14:48', '2020-05-19 08:14:48');
INSERT INTO `module_fields` VALUES ('178', 'requirements_ru', 'requirements ru', '36', '21', '0', '', '0', '0', '0', '', '30', '2020-05-19 08:15:08', '2020-05-19 08:15:08');
INSERT INTO `module_fields` VALUES ('179', 'transport', 'transport', '36', '7', '0', '', '0', '0', '0', '[\"yes\",\"no\"]', '24', '2020-05-19 08:21:21', '2020-05-19 08:21:21');
INSERT INTO `module_fields` VALUES ('180', 'image', 'image', '37', '12', '0', '', '0', '0', '0', '', '0', '2020-05-19 08:26:03', '2020-05-19 08:26:03');
INSERT INTO `module_fields` VALUES ('181', 'slogan_az', 'slogan az', '32', '22', '0', '', '0', '256', '0', '', '0', '2020-05-20 12:49:22', '2020-05-20 12:49:22');
INSERT INTO `module_fields` VALUES ('182', 'slogan_en', 'slogan en', '32', '22', '0', '', '0', '256', '0', '', '0', '2020-05-20 12:49:42', '2020-05-20 12:49:42');
INSERT INTO `module_fields` VALUES ('183', 'slogan_ru', 'slogan ru', '32', '22', '0', '', '0', '256', '0', '', '0', '2020-05-20 12:50:04', '2020-05-20 12:50:04');
INSERT INTO `module_fields` VALUES ('184', 'name_en', 'name en', '23', '22', '0', '', '0', '256', '0', '', '2', '2020-05-20 13:11:46', '2020-05-20 13:11:46');
INSERT INTO `module_fields` VALUES ('185', 'name_ru', 'name ru', '23', '22', '0', '', '0', '256', '0', '', '3', '2020-05-20 13:13:48', '2020-05-20 13:13:48');

-- ----------------------------
-- Table structure for module_field_types
-- ----------------------------
DROP TABLE IF EXISTS `module_field_types`;
CREATE TABLE `module_field_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of module_field_types
-- ----------------------------
INSERT INTO `module_field_types` VALUES ('1', 'Address', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('2', 'Checkbox', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('3', 'Currency', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('4', 'Date', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('5', 'Datetime', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('6', 'Decimal', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('7', 'Dropdown', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('8', 'Email', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('9', 'File', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('10', 'Float', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('11', 'HTML', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('12', 'Image', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('13', 'Integer', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('14', 'Mobile', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('15', 'Multiselect', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('16', 'Name', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('17', 'Password', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('18', 'Radio', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('19', 'String', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('20', 'Taginput', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('21', 'Textarea', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('22', 'TextField', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('23', 'URL', '2020-05-01 10:35:04', '2020-05-01 10:35:04');
INSERT INTO `module_field_types` VALUES ('24', 'Files', '2020-05-01 10:35:04', '2020-05-01 10:35:04');

-- ----------------------------
-- Table structure for organizations
-- ----------------------------
DROP TABLE IF EXISTS `organizations`;
CREATE TABLE `organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://',
  `assigned_to` int(10) unsigned NOT NULL DEFAULT 1,
  `connect_since` date NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organizations_name_unique` (`name`),
  UNIQUE KEY `organizations_email_unique` (`email`),
  KEY `organizations_assigned_to_foreign` (`assigned_to`),
  CONSTRAINT `organizations_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of organizations
-- ----------------------------

-- ----------------------------
-- Table structure for our_team_images
-- ----------------------------
DROP TABLE IF EXISTS `our_team_images`;
CREATE TABLE `our_team_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `leadership_image` int(11) NOT NULL,
  `people_image` int(11) NOT NULL,
  `expert_image` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of our_team_images
-- ----------------------------
INSERT INTO `our_team_images` VALUES ('1', null, '2020-05-03 13:52:34', '2020-05-07 09:30:35', '7', '6', '6', '6');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', null, '2020-05-01 10:35:09', '2020-05-01 10:35:09');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for planning_and_scheds
-- ----------------------------
DROP TABLE IF EXISTS `planning_and_scheds`;
CREATE TABLE `planning_and_scheds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of planning_and_scheds
-- ----------------------------
INSERT INTO `planning_and_scheds` VALUES ('1', null, '2020-05-05 04:27:31', '2020-05-05 04:27:31', '12', '', '<p class=\"MsoNormal\">Project Management.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PMIS Project Management team\r\ncomprise a group of dedicated and experienced project management professionals\r\ndelivering high quality schemes across a selection of Client sectors throughout\r\nAzerbaijan.</span><span style=\"font-size:9.0pt;line-height:107%;font-family:\r\n\"Arial\",sans-serif;color:black\"><br>\r\n<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Our team provide Project Management and Contract\r\nAdministration services in relation to greenfield and brownfield turnkey\r\nprojects from Strategic planning through to project delivery. Our aim is to add\r\nvalue and manage risk by working with Clients with our transactional teams to\r\nprovide a comprehensive “one stop” capability.<o:p></o:p></span></span></p>', '');

-- ----------------------------
-- Table structure for project_serv_sols
-- ----------------------------
DROP TABLE IF EXISTS `project_serv_sols`;
CREATE TABLE `project_serv_sols` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of project_serv_sols
-- ----------------------------
INSERT INTO `project_serv_sols` VALUES ('1', null, '2020-05-03 11:29:33', '2020-05-06 18:27:55', '16', '<p class=\"MsoNormal\">Project Management.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PMIS Project Management team\r\ncomprise a group of dedicated and experienced project management professionals\r\ndelivering high quality schemes across a selection of Client sectors throughout\r\nAzerbaijan.</span><span style=\"font-size:9.0pt;line-height:107%;font-family:\r\n\" arial\",sans-serif;color:black\"=\"\"><br>\r\n<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Our team provide Project Management and Contract\r\nAdministration services in relation to greenfield and brownfield turnkey\r\nprojects from Strategic planning through to project delivery. Our aim is to add\r\nvalue and manage risk by working with Clients with our transactional teams to\r\nprovide a comprehensive “one stop” capability.</span></span><o:p></o:p></p>', '<div class=\"page-subtitle__title\" style=\"box-sizing: inherit; color: rgb(173, 70, 153); font-size: 1.25em; font-family: Montserrat, sans-serif;\"><div class=\"page-subtitle__title\" style=\"box-sizing: inherit; font-size: 1.25em;\"><em style=\"box-sizing: inherit;\">rhi</em> continues to evolve its services and systems to provide relevant support for clients as they focus on creating a lower carbon future, sustainability and building resilience in their businesses.</div><div class=\"page-subtitle__text\" style=\"box-sizing: inherit; font-size: 1.125em; padding-bottom: 1.25em; color: rgb(0, 54, 95); line-height: 2.0625rem;\"><p style=\"box-sizing: inherit;\">Our commitment to growth and development is driving innovation in <em style=\"box-sizing: inherit;\">rhi’s</em> methods of delivery and organisation.</p><p style=\"box-sizing: inherit;\">Across all phases of an assets’ life cycle, from strategy development and feasibility studies to project execution, operations and decommissioning, <em style=\"box-sizing: inherit;\">rhi</em> supports the clients’ decision-making process. Our experience includes upstream, downstream and mid-stream for both greenfield and life extension projects, as well as the ability deliver to energy and natural resources sectors.</p><h3 style=\"box-sizing: inherit; font-size: 1.9375rem; margin-bottom: 0px; padding-bottom: 0.9375rem; color: rgb(173, 70, 153);\">Our Capabilities:</h3></div></div>', '<p class=\"MsoNormal\">Project Management.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PMIS Project Management team\r\ncomprise a group of dedicated and experienced project management professionals\r\ndelivering high quality schemes across a selection of Client sectors throughout\r\nAzerbaijan.</span><span style=\"font-size:9.0pt;line-height:107%;font-family:\r\n\" arial\",sans-serif;color:black\"=\"\"><br>\r\n<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Our team provide Project Management and Contract\r\nAdministration services in relation to greenfield and brownfield turnkey\r\nprojects from Strategic planning through to project delivery. Our aim is to add\r\nvalue and manage risk by working with Clients with our transactional teams to\r\nprovide a comprehensive “one stop” capability.</span></span><o:p></o:p></p>');

-- ----------------------------
-- Table structure for prop_sp_and_cost_ests
-- ----------------------------
DROP TABLE IF EXISTS `prop_sp_and_cost_ests`;
CREATE TABLE `prop_sp_and_cost_ests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of prop_sp_and_cost_ests
-- ----------------------------
INSERT INTO `prop_sp_and_cost_ests` VALUES ('1', null, '2020-05-01 12:52:25', '2020-05-06 17:35:26', '11', '<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en;mso-bidi-font-weight:bold\"=\"\">FƏALİYYƏT DAİRƏMİZ<span style=\"color:red\"><o:p></o:p></span></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Risk və imkanların idarə olunması, Dəyişikliklərin idarə\r\nolunması</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Planlama və cədvəlin hazırlanması; İşlərin gedişatı barədə\r\nhesabat və işlərin gedişatının vəziyyətinə dair iclaslar</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Resursların idarə olunması, xərclərə nəzarət və budcənin\r\nyenilənməsi</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Tender, Qiymətləndirmə və Mükafatlandırma üzrə Tövsiyyələr</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi-language:\r\nEN\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Smetaçı və Müqavilələrə\r\nmaliyyə dəstəyi xidmətləri<o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 24.7pt; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;color:red;mso-ansi-language:en\"=\"\"><o:p> </o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en;mso-bidi-font-weight:bold\"=\"\">DƏYƏRLƏRİMİZ<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Təsir</span></b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en;mso-bidi-font-weight:=\"\" bold\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Müstəsna nəticələr əldə etmək üçün səylərimizi əsirgəmirik – biz müştərilərimizlə\r\nbirlikdə uğur qazanırıq.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Mükəmməllik </span></b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;color:red;=\"\" mso-ansi-language:en\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Müştərilərimizə əla iş təcrübəsi\r\nvə keyfiyyət təklif etmək üçün bacarıq və imkanlarımızı daim təkmilləşdiririk.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Hörmət</span></b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" color:red;mso-ansi-language:en\"=\"\">. <o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Biz  əməkdaşlarımıza , müştərilərimizə və digərlərinə\r\nbərabər səviyyədə və hörmətlə </span><span lang=\"AZ-LATIN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" az-latin\"=\"\">yanaşırıq</span><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">. Bizim məramımiz hər\r\nbir neft, qaz və neft-kimya İnşaat layihəsinin işlərin əsas planlama cədvəli  və təsdiq edilmiş büdcə çərçivəsində rəqabətlə,\r\nyüksək keyfiyyət standartlarına uyğun olaraq yerinə yetirilməsini təmin etməkdirKomandamız\r\nbu məramımıza uyğun olaraq davranış sərgiləyir və buna  insanlara və ətraf mühitə zərər vermədən nail\r\nolmaq üçün səylərini əsirgəmir.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Missiyamız<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Bizim missiyamız yerli kadrlara dəstək olmaqla , həmçinin məzunların və rəqiblərin\r\nbacarıq və imkanlarını artırmaqla Azərbaycanın gələcəyini qurmaqdır<o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en;mso-bidi-font-weight:bold\"=\"\">FƏALİYYƏT DAİRƏMİZ<span style=\"color:red\"><o:p></o:p></span></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Risk və imkanların idarə olunması, Dəyişikliklərin idarə\r\nolunması</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Planlama və cədvəlin hazırlanması; İşlərin gedişatı barədə\r\nhesabat və işlərin gedişatının vəziyyətinə dair iclaslar</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Resursların idarə olunması, xərclərə nəzarət və budcənin\r\nyenilənməsi</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Tender, Qiymətləndirmə və Mükafatlandırma üzrə Tövsiyyələr</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi-language:\r\nEN\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Smetaçı və Müqavilələrə\r\nmaliyyə dəstəyi xidmətləri<o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 24.7pt; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;color:red;mso-ansi-language:en\"=\"\"><o:p> </o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en;mso-bidi-font-weight:bold\"=\"\">DƏYƏRLƏRİMİZ<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Təsir</span></b></p><p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size: 9pt;\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">sdkjlksajfhdlkasjdf</span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Mükəmməllik </span></b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;color:red;=\"\" mso-ansi-language:en\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Müştərilərimizə əla iş təcrübəsi\r\nvə keyfiyyət təklif etmək üçün bacarıq və imkanlarımızı daim təkmilləşdiririk.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Hörmət</span></b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" color:red;mso-ansi-language:en\"=\"\">. <o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Biz  əməkdaşlarımıza , müştərilərimizə və digərlərinə\r\nbərabər səviyyədə və hörmətlə </span><span lang=\"AZ-LATIN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" az-latin\"=\"\">yanaşırıq</span><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">. Bizim məramımiz hər\r\nbir neft, qaz və neft-kimya İnşaat layihəsinin işlərin əsas planlama cədvəli  və təsdiq edilmiş büdcə çərçivəsində rəqabətlə,\r\nyüksək keyfiyyət standartlarına uyğun olaraq yerinə yetirilməsini təmin etməkdirKomandamız\r\nbu məramımıza uyğun olaraq davranış sərgiləyir və buna  insanlara və ətraf mühitə zərər vermədən nail\r\nolmaq üçün səylərini əsirgəmir.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Missiyamız<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Bizim missiyamız yerli kadrlara dəstək olmaqla , həmçinin məzunların və rəqiblərin\r\nbacarıq və imkanlarını artırmaqla Azərbaycanın gələcəyini qurmaqdır<o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en;mso-bidi-font-weight:bold\"=\"\">FƏALİYYƏT DAİRƏMİZ<span style=\"color:red\"><o:p></o:p></span></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Risk və imkanların idarə olunması, Dəyişikliklərin idarə\r\nolunması</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Planlama və cədvəlin hazırlanması; İşlərin gedişatı barədə\r\nhesabat və işlərin gedişatının vəziyyətinə dair iclaslar</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Resursların idarə olunması, xərclərə nəzarət və budcənin\r\nyenilənməsi</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">  </span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Tender, Qiymətləndirmə və Mükafatlandırma üzrə Tövsiyyələr</span><span style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 0.5in; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:Wingdings;\r\nmso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi-language:\r\nEN\">Ø<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\"> \r\n</span></span><!--[endif]--><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Smetaçı və Müqavilələrə\r\nmaliyyə dəstəyi xidmətləri<o:p></o:p></span></p>\r\n\r\n<p class=\"xmsonormal\" style=\"margin: 0in 0in 0.0001pt 24.7pt; text-indent: -0.25in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;color:red;mso-ansi-language:en\"=\"\"><o:p> </o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en;mso-bidi-font-weight:bold\"=\"\">DƏYƏRLƏRİMİZ<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Təsir</span></b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en;mso-bidi-font-weight:=\"\" bold\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Müstəsna nəticələr əldə etmək üçün səylərimizi əsirgəmirik – biz müştərilərimizlə\r\nbirlikdə uğur qazanırıq.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" mso-ansi-language:en\"=\"\">Mükəmməllik </span></b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;color:red;=\"\" mso-ansi-language:en\"=\"\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Müştərilərimizə əla iş təcrübəsi\r\nvə keyfiyyət təklif etmək üçün bacarıq və imkanlarımızı daim təkmilləşdiririk.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Hörmət</span></b><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;=\"\" color:red;mso-ansi-language:en\"=\"\">. <o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:11.0pt;\r\nfont-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">Biz  əməkdaşlarımıza , müştərilərimizə və digərlərinə\r\nbərabər səviyyədə və hörmətlə </span><span lang=\"AZ-LATIN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" az-latin\"=\"\">yanaşırıq</span><span lang=\"EN\" style=\"font-size:9.0pt;mso-bidi-font-size:\r\n11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:en\"=\"\">. Bizim məramımiz hər\r\nbir neft, qaz və neft-kimya İnşaat layihəsinin işlərin əsas planlama cədvəli  və təsdiq edilmiş büdcə çərçivəsində rəqabətlə,\r\nyüksək keyfiyyət standartlarına uyğun olaraq yerinə yetirilməsini təmin etməkdirKomandamız\r\nbu məramımıza uyğun olaraq davranış sərgiləyir və buna  insanlara və ətraf mühitə zərər vermədən nail\r\nolmaq üçün səylərini əsirgəmir.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"EN\" style=\"font-size:\r\n9.0pt;mso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Missiyamız<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin: 12pt 0in 22.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"EN\" style=\"font-size:9.0pt;\r\nmso-bidi-font-size:11.0pt;font-family:\" arial\",sans-serif;mso-ansi-language:=\"\" en\"=\"\">Bizim missiyamız yerli kadrlara dəstək olmaqla , həmçinin məzunların və rəqiblərin\r\nbacarıq və imkanlarını artırmaqla Azərbaycanın gələcəyini qurmaqdır<o:p></o:p></span></p>');

-- ----------------------------
-- Table structure for quality_s_and_con_ads
-- ----------------------------
DROP TABLE IF EXISTS `quality_s_and_con_ads`;
CREATE TABLE `quality_s_and_con_ads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of quality_s_and_con_ads
-- ----------------------------
INSERT INTO `quality_s_and_con_ads` VALUES ('1', null, '2020-05-05 04:28:12', '2020-05-05 04:28:12', '11', '', '<p class=\"MsoNormal\">Project Management.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PMIS Project Management team\r\ncomprise a group of dedicated and experienced project management professionals\r\ndelivering high quality schemes across a selection of Client sectors throughout\r\nAzerbaijan.</span><span style=\"font-size:9.0pt;line-height:107%;font-family:\r\n\"Arial\",sans-serif;color:black\"><br>\r\n<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Our team provide Project Management and Contract\r\nAdministration services in relation to greenfield and brownfield turnkey\r\nprojects from Strategic planning through to project delivery. Our aim is to add\r\nvalue and manage risk by working with Clients with our transactional teams to\r\nprovide a comprehensive “one stop” capability.<o:p></o:p></span></span></p>', '');

-- ----------------------------
-- Table structure for reporting_and_autos
-- ----------------------------
DROP TABLE IF EXISTS `reporting_and_autos`;
CREATE TABLE `reporting_and_autos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of reporting_and_autos
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) unsigned NOT NULL DEFAULT 1,
  `dept` int(10) unsigned NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  KEY `roles_parent_foreign` (`parent`),
  KEY `roles_dept_foreign` (`dept`),
  CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', '1', '1', null, '2020-05-01 10:35:09', '2020-05-01 10:35:09');

-- ----------------------------
-- Table structure for role_module
-- ----------------------------
DROP TABLE IF EXISTS `role_module`;
CREATE TABLE `role_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_module_role_id_foreign` (`role_id`),
  KEY `role_module_module_id_foreign` (`module_id`),
  CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_module
-- ----------------------------
INSERT INTO `role_module` VALUES ('1', '1', '1', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('2', '1', '2', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('3', '1', '3', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('4', '1', '4', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('5', '1', '5', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('6', '1', '6', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('7', '1', '7', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('8', '1', '8', '1', '1', '1', '1', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module` VALUES ('12', '1', '15', '1', '1', '1', '1', '2020-05-01 11:36:20', '2020-05-01 11:36:20');
INSERT INTO `role_module` VALUES ('13', '1', '16', '1', '1', '1', '1', '2020-05-01 13:06:54', '2020-05-01 13:06:54');
INSERT INTO `role_module` VALUES ('14', '1', '17', '1', '1', '1', '1', '2020-05-01 13:16:45', '2020-05-01 13:16:45');
INSERT INTO `role_module` VALUES ('15', '1', '18', '1', '1', '1', '1', '2020-05-01 13:19:59', '2020-05-01 13:19:59');
INSERT INTO `role_module` VALUES ('16', '1', '19', '1', '1', '1', '1', '2020-05-01 13:23:43', '2020-05-01 13:23:43');
INSERT INTO `role_module` VALUES ('17', '1', '20', '1', '1', '1', '1', '2020-05-01 13:26:41', '2020-05-01 13:26:41');
INSERT INTO `role_module` VALUES ('18', '1', '21', '1', '1', '1', '1', '2020-05-01 13:30:04', '2020-05-01 13:30:04');
INSERT INTO `role_module` VALUES ('19', '1', '22', '1', '1', '1', '1', '2020-05-01 13:34:10', '2020-05-01 13:34:10');
INSERT INTO `role_module` VALUES ('20', '1', '23', '1', '1', '1', '1', '2020-05-01 20:32:51', '2020-05-01 20:32:51');
INSERT INTO `role_module` VALUES ('21', '1', '24', '1', '1', '1', '1', '2020-05-01 20:48:15', '2020-05-01 20:48:15');
INSERT INTO `role_module` VALUES ('22', '1', '25', '1', '1', '1', '1', '2020-05-01 21:06:28', '2020-05-01 21:06:28');
INSERT INTO `role_module` VALUES ('23', '1', '26', '1', '1', '1', '1', '2020-05-03 11:16:55', '2020-05-03 11:16:55');
INSERT INTO `role_module` VALUES ('24', '1', '27', '1', '1', '1', '1', '2020-05-03 13:23:12', '2020-05-03 13:23:12');
INSERT INTO `role_module` VALUES ('26', '1', '31', '1', '1', '1', '1', '2020-05-04 18:58:09', '2020-05-04 18:58:09');
INSERT INTO `role_module` VALUES ('27', '1', '32', '1', '1', '1', '1', '2020-05-16 11:29:07', '2020-05-16 11:29:07');
INSERT INTO `role_module` VALUES ('28', '1', '33', '1', '1', '1', '1', '2020-05-16 11:31:52', '2020-05-16 11:31:52');
INSERT INTO `role_module` VALUES ('31', '1', '36', '1', '1', '1', '1', '2020-05-19 07:08:37', '2020-05-19 07:08:37');
INSERT INTO `role_module` VALUES ('32', '1', '37', '1', '1', '1', '1', '2020-05-19 08:27:00', '2020-05-19 08:27:00');

-- ----------------------------
-- Table structure for role_module_fields
-- ----------------------------
DROP TABLE IF EXISTS `role_module_fields`;
CREATE TABLE `role_module_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `field_id` int(10) unsigned NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_module_fields_role_id_foreign` (`role_id`),
  KEY `role_module_fields_field_id_foreign` (`field_id`),
  CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_module_fields
-- ----------------------------
INSERT INTO `role_module_fields` VALUES ('1', '1', '1', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('2', '1', '2', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('3', '1', '3', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('4', '1', '4', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('5', '1', '5', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('6', '1', '6', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('7', '1', '7', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('8', '1', '8', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('9', '1', '9', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('10', '1', '10', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('11', '1', '11', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('12', '1', '12', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('13', '1', '13', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('14', '1', '14', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('15', '1', '15', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('16', '1', '16', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('17', '1', '17', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('18', '1', '18', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('19', '1', '19', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('20', '1', '20', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('21', '1', '21', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('22', '1', '22', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('23', '1', '23', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('24', '1', '24', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('25', '1', '25', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('26', '1', '26', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('27', '1', '27', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('28', '1', '28', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('29', '1', '29', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('30', '1', '30', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('31', '1', '31', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('32', '1', '32', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('33', '1', '33', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('34', '1', '34', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('35', '1', '35', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('36', '1', '36', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('37', '1', '37', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('38', '1', '38', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('39', '1', '39', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('40', '1', '40', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('41', '1', '41', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('42', '1', '42', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('43', '1', '43', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('44', '1', '44', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('45', '1', '45', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('46', '1', '46', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('47', '1', '47', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('48', '1', '48', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('49', '1', '49', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('50', '1', '50', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('51', '1', '51', 'write', '2020-05-01 10:35:09', '2020-05-01 10:35:09');
INSERT INTO `role_module_fields` VALUES ('68', '1', '68', 'write', '2020-05-01 11:37:49', '2020-05-01 11:37:49');
INSERT INTO `role_module_fields` VALUES ('69', '1', '69', 'write', '2020-05-01 11:38:25', '2020-05-01 11:38:25');
INSERT INTO `role_module_fields` VALUES ('70', '1', '70', 'write', '2020-05-01 11:38:48', '2020-05-01 11:38:48');
INSERT INTO `role_module_fields` VALUES ('71', '1', '71', 'write', '2020-05-01 11:39:08', '2020-05-01 11:39:08');
INSERT INTO `role_module_fields` VALUES ('72', '1', '72', 'write', '2020-05-01 13:06:42', '2020-05-01 13:06:42');
INSERT INTO `role_module_fields` VALUES ('73', '1', '73', 'write', '2020-05-01 13:08:56', '2020-05-01 13:08:56');
INSERT INTO `role_module_fields` VALUES ('74', '1', '74', 'write', '2020-05-01 13:09:34', '2020-05-01 13:09:34');
INSERT INTO `role_module_fields` VALUES ('75', '1', '75', 'write', '2020-05-01 13:09:59', '2020-05-01 13:09:59');
INSERT INTO `role_module_fields` VALUES ('76', '1', '76', 'write', '2020-05-01 13:14:38', '2020-05-01 13:14:38');
INSERT INTO `role_module_fields` VALUES ('77', '1', '77', 'write', '2020-05-01 13:15:15', '2020-05-01 13:15:15');
INSERT INTO `role_module_fields` VALUES ('78', '1', '78', 'write', '2020-05-01 13:15:33', '2020-05-01 13:15:33');
INSERT INTO `role_module_fields` VALUES ('79', '1', '79', 'write', '2020-05-01 13:15:56', '2020-05-01 13:15:56');
INSERT INTO `role_module_fields` VALUES ('80', '1', '80', 'write', '2020-05-01 13:18:16', '2020-05-01 13:18:16');
INSERT INTO `role_module_fields` VALUES ('81', '1', '81', 'write', '2020-05-01 13:18:43', '2020-05-01 13:18:43');
INSERT INTO `role_module_fields` VALUES ('82', '1', '82', 'write', '2020-05-01 13:19:13', '2020-05-01 13:19:13');
INSERT INTO `role_module_fields` VALUES ('83', '1', '83', 'write', '2020-05-01 13:19:35', '2020-05-01 13:19:35');
INSERT INTO `role_module_fields` VALUES ('84', '1', '84', 'write', '2020-05-01 13:22:34', '2020-05-01 13:22:34');
INSERT INTO `role_module_fields` VALUES ('85', '1', '85', 'write', '2020-05-01 13:22:50', '2020-05-01 13:22:50');
INSERT INTO `role_module_fields` VALUES ('86', '1', '86', 'write', '2020-05-01 13:23:08', '2020-05-01 13:23:08');
INSERT INTO `role_module_fields` VALUES ('87', '1', '87', 'write', '2020-05-01 13:23:34', '2020-05-01 13:23:34');
INSERT INTO `role_module_fields` VALUES ('88', '1', '88', 'write', '2020-05-01 13:24:48', '2020-05-01 13:24:48');
INSERT INTO `role_module_fields` VALUES ('89', '1', '89', 'write', '2020-05-01 13:25:18', '2020-05-01 13:25:18');
INSERT INTO `role_module_fields` VALUES ('90', '1', '90', 'write', '2020-05-01 13:25:32', '2020-05-01 13:25:32');
INSERT INTO `role_module_fields` VALUES ('91', '1', '91', 'write', '2020-05-01 13:26:25', '2020-05-01 13:26:25');
INSERT INTO `role_module_fields` VALUES ('92', '1', '92', 'write', '2020-05-01 13:28:10', '2020-05-01 13:28:10');
INSERT INTO `role_module_fields` VALUES ('93', '1', '93', 'write', '2020-05-01 13:28:26', '2020-05-01 13:28:26');
INSERT INTO `role_module_fields` VALUES ('94', '1', '94', 'write', '2020-05-01 13:28:40', '2020-05-01 13:28:40');
INSERT INTO `role_module_fields` VALUES ('95', '1', '95', 'write', '2020-05-01 13:29:03', '2020-05-01 13:29:03');
INSERT INTO `role_module_fields` VALUES ('96', '1', '96', 'write', '2020-05-01 13:34:01', '2020-05-01 13:34:01');
INSERT INTO `role_module_fields` VALUES ('97', '1', '97', 'write', '2020-05-01 13:34:31', '2020-05-01 13:34:31');
INSERT INTO `role_module_fields` VALUES ('98', '1', '98', 'write', '2020-05-01 13:34:50', '2020-05-01 13:34:50');
INSERT INTO `role_module_fields` VALUES ('99', '1', '99', 'write', '2020-05-01 13:35:04', '2020-05-01 13:35:04');
INSERT INTO `role_module_fields` VALUES ('100', '1', '100', 'write', '2020-05-01 20:28:26', '2020-05-01 20:28:26');
INSERT INTO `role_module_fields` VALUES ('101', '1', '101', 'write', '2020-05-01 20:29:31', '2020-05-01 20:29:31');
INSERT INTO `role_module_fields` VALUES ('103', '1', '103', 'write', '2020-05-01 20:30:29', '2020-05-01 20:30:29');
INSERT INTO `role_module_fields` VALUES ('104', '1', '104', 'write', '2020-05-01 20:31:11', '2020-05-01 20:31:11');
INSERT INTO `role_module_fields` VALUES ('105', '1', '105', 'write', '2020-05-01 20:43:20', '2020-05-01 20:43:20');
INSERT INTO `role_module_fields` VALUES ('106', '1', '106', 'write', '2020-05-01 20:45:58', '2020-05-01 20:45:58');
INSERT INTO `role_module_fields` VALUES ('107', '1', '107', 'write', '2020-05-01 20:46:18', '2020-05-01 20:46:18');
INSERT INTO `role_module_fields` VALUES ('108', '1', '108', 'write', '2020-05-01 20:48:02', '2020-05-01 20:48:02');
INSERT INTO `role_module_fields` VALUES ('109', '1', '109', 'write', '2020-05-01 20:50:18', '2020-05-01 20:50:18');
INSERT INTO `role_module_fields` VALUES ('110', '1', '110', 'write', '2020-05-01 20:50:56', '2020-05-01 20:50:56');
INSERT INTO `role_module_fields` VALUES ('111', '1', '111', 'write', '2020-05-01 20:51:17', '2020-05-01 20:51:17');
INSERT INTO `role_module_fields` VALUES ('112', '1', '112', 'write', '2020-05-01 21:05:31', '2020-05-01 21:05:31');
INSERT INTO `role_module_fields` VALUES ('113', '1', '113', 'write', '2020-05-01 21:06:11', '2020-05-01 21:06:11');
INSERT INTO `role_module_fields` VALUES ('114', '1', '114', 'write', '2020-05-03 11:15:19', '2020-05-03 11:15:19');
INSERT INTO `role_module_fields` VALUES ('115', '1', '115', 'write', '2020-05-03 11:15:43', '2020-05-03 11:15:43');
INSERT INTO `role_module_fields` VALUES ('116', '1', '116', 'write', '2020-05-03 11:16:07', '2020-05-03 11:16:07');
INSERT INTO `role_module_fields` VALUES ('117', '1', '117', 'write', '2020-05-03 11:16:39', '2020-05-03 11:16:39');
INSERT INTO `role_module_fields` VALUES ('118', '1', '118', 'write', '2020-05-03 12:55:41', '2020-05-03 12:55:41');
INSERT INTO `role_module_fields` VALUES ('120', '1', '120', 'write', '2020-05-03 13:23:03', '2020-05-03 13:23:03');
INSERT INTO `role_module_fields` VALUES ('126', '1', '127', 'write', '2020-05-04 18:57:56', '2020-05-04 18:57:56');
INSERT INTO `role_module_fields` VALUES ('127', '1', '128', 'write', '2020-05-04 18:58:41', '2020-05-04 18:58:41');
INSERT INTO `role_module_fields` VALUES ('128', '1', '129', 'write', '2020-05-04 18:58:58', '2020-05-04 18:58:58');
INSERT INTO `role_module_fields` VALUES ('129', '1', '130', 'write', '2020-05-04 18:59:28', '2020-05-04 18:59:28');
INSERT INTO `role_module_fields` VALUES ('130', '1', '131', 'write', '2020-05-07 07:39:11', '2020-05-07 07:39:11');
INSERT INTO `role_module_fields` VALUES ('131', '1', '132', 'write', '2020-05-07 09:06:46', '2020-05-07 09:06:46');
INSERT INTO `role_module_fields` VALUES ('132', '1', '133', 'write', '2020-05-07 09:07:18', '2020-05-07 09:07:18');
INSERT INTO `role_module_fields` VALUES ('133', '1', '134', 'write', '2020-05-07 09:08:16', '2020-05-07 09:08:16');
INSERT INTO `role_module_fields` VALUES ('134', '1', '135', 'write', '2020-05-16 11:21:00', '2020-05-16 11:21:00');
INSERT INTO `role_module_fields` VALUES ('135', '1', '136', 'write', '2020-05-16 11:23:41', '2020-05-16 11:23:41');
INSERT INTO `role_module_fields` VALUES ('136', '1', '137', 'write', '2020-05-16 11:24:00', '2020-05-16 11:24:00');
INSERT INTO `role_module_fields` VALUES ('137', '1', '138', 'write', '2020-05-16 11:24:18', '2020-05-16 11:24:18');
INSERT INTO `role_module_fields` VALUES ('141', '1', '142', 'write', '2020-05-16 11:30:48', '2020-05-16 11:30:48');
INSERT INTO `role_module_fields` VALUES ('142', '1', '143', 'write', '2020-05-16 11:31:09', '2020-05-16 11:31:09');
INSERT INTO `role_module_fields` VALUES ('143', '1', '144', 'write', '2020-05-16 11:31:24', '2020-05-16 11:31:24');
INSERT INTO `role_module_fields` VALUES ('149', '1', '150', 'write', '2020-05-19 07:08:27', '2020-05-19 07:08:27');
INSERT INTO `role_module_fields` VALUES ('150', '1', '151', 'write', '2020-05-19 07:14:36', '2020-05-19 07:14:36');
INSERT INTO `role_module_fields` VALUES ('151', '1', '152', 'write', '2020-05-19 07:14:55', '2020-05-19 07:14:55');
INSERT INTO `role_module_fields` VALUES ('152', '1', '153', 'write', '2020-05-19 07:15:27', '2020-05-19 07:15:27');
INSERT INTO `role_module_fields` VALUES ('153', '1', '154', 'write', '2020-05-19 07:16:09', '2020-05-19 07:16:09');
INSERT INTO `role_module_fields` VALUES ('154', '1', '155', 'write', '2020-05-19 07:16:43', '2020-05-19 07:16:43');
INSERT INTO `role_module_fields` VALUES ('155', '1', '156', 'write', '2020-05-19 07:18:51', '2020-05-19 07:18:51');
INSERT INTO `role_module_fields` VALUES ('156', '1', '157', 'write', '2020-05-19 07:19:50', '2020-05-19 07:19:50');
INSERT INTO `role_module_fields` VALUES ('157', '1', '158', 'write', '2020-05-19 07:20:16', '2020-05-19 07:20:16');
INSERT INTO `role_module_fields` VALUES ('158', '1', '159', 'write', '2020-05-19 07:21:25', '2020-05-19 07:21:25');
INSERT INTO `role_module_fields` VALUES ('159', '1', '160', 'write', '2020-05-19 07:22:29', '2020-05-19 07:22:29');
INSERT INTO `role_module_fields` VALUES ('160', '1', '161', 'write', '2020-05-19 07:23:19', '2020-05-19 07:23:19');
INSERT INTO `role_module_fields` VALUES ('161', '1', '162', 'write', '2020-05-19 07:23:46', '2020-05-19 07:23:46');
INSERT INTO `role_module_fields` VALUES ('162', '1', '163', 'write', '2020-05-19 07:25:41', '2020-05-19 07:25:41');
INSERT INTO `role_module_fields` VALUES ('163', '1', '164', 'write', '2020-05-19 07:26:02', '2020-05-19 07:26:02');
INSERT INTO `role_module_fields` VALUES ('164', '1', '165', 'write', '2020-05-19 07:26:24', '2020-05-19 07:26:24');
INSERT INTO `role_module_fields` VALUES ('165', '1', '166', 'write', '2020-05-19 07:27:02', '2020-05-19 07:27:02');
INSERT INTO `role_module_fields` VALUES ('166', '1', '167', 'write', '2020-05-19 07:28:10', '2020-05-19 07:28:10');
INSERT INTO `role_module_fields` VALUES ('167', '1', '168', 'write', '2020-05-19 07:28:41', '2020-05-19 07:28:41');
INSERT INTO `role_module_fields` VALUES ('168', '1', '169', 'write', '2020-05-19 07:29:03', '2020-05-19 07:29:03');
INSERT INTO `role_module_fields` VALUES ('169', '1', '170', 'write', '2020-05-19 08:08:26', '2020-05-19 08:08:26');
INSERT INTO `role_module_fields` VALUES ('170', '1', '171', 'write', '2020-05-19 08:08:52', '2020-05-19 08:08:52');
INSERT INTO `role_module_fields` VALUES ('171', '1', '172', 'write', '2020-05-19 08:09:45', '2020-05-19 08:09:45');
INSERT INTO `role_module_fields` VALUES ('172', '1', '173', 'write', '2020-05-19 08:12:10', '2020-05-19 08:12:10');
INSERT INTO `role_module_fields` VALUES ('173', '1', '174', 'write', '2020-05-19 08:12:36', '2020-05-19 08:12:36');
INSERT INTO `role_module_fields` VALUES ('174', '1', '175', 'write', '2020-05-19 08:13:18', '2020-05-19 08:13:18');
INSERT INTO `role_module_fields` VALUES ('175', '1', '176', 'write', '2020-05-19 08:14:27', '2020-05-19 08:14:27');
INSERT INTO `role_module_fields` VALUES ('176', '1', '177', 'write', '2020-05-19 08:14:48', '2020-05-19 08:14:48');
INSERT INTO `role_module_fields` VALUES ('177', '1', '178', 'write', '2020-05-19 08:15:08', '2020-05-19 08:15:08');
INSERT INTO `role_module_fields` VALUES ('178', '1', '179', 'write', '2020-05-19 08:21:21', '2020-05-19 08:21:21');
INSERT INTO `role_module_fields` VALUES ('179', '1', '180', 'write', '2020-05-19 08:26:03', '2020-05-19 08:26:03');
INSERT INTO `role_module_fields` VALUES ('180', '1', '181', 'write', '2020-05-20 12:49:23', '2020-05-20 12:49:23');
INSERT INTO `role_module_fields` VALUES ('181', '1', '182', 'write', '2020-05-20 12:49:42', '2020-05-20 12:49:42');
INSERT INTO `role_module_fields` VALUES ('182', '1', '183', 'write', '2020-05-20 12:50:04', '2020-05-20 12:50:04');
INSERT INTO `role_module_fields` VALUES ('183', '1', '184', 'write', '2020-05-20 13:11:46', '2020-05-20 13:11:46');
INSERT INTO `role_module_fields` VALUES ('184', '1', '185', 'write', '2020-05-20 13:13:48', '2020-05-20 13:13:48');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '1', '1', null, null);

-- ----------------------------
-- Table structure for team_members
-- ----------------------------
DROP TABLE IF EXISTS `team_members`;
CREATE TABLE `team_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` int(11) NOT NULL,
  `mail` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `linkedIn` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `member_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '''[""]''',
  `name_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of team_members
-- ----------------------------
INSERT INTO `team_members` VALUES ('10', null, '2020-05-20 13:16:22', '2020-05-20 13:16:22', 'Vüsal Hüseynov', '6', 'vusal.huseynov@pmis.az', '', 'Baş Direktor', 'General Director', 'Генеральный Директор', 'OUR LEADERSHIP', 'Vusal Huseynov', 'Вюсал Гусейнов');
INSERT INTO `team_members` VALUES ('11', null, '2020-05-20 13:17:30', '2020-05-20 13:17:30', 'Camal Məmmədrzayev', '6', 'jamal.mammadrzayev@pmis.az', 'https://www.linkedin.com/mwlite/in/jamal-mammadrzayev-12b28550', 'Biznes Xidmətləri Rəhbəri', 'Business Services Manager', 'Руководитель Коммерческих Услуг', 'OUR LEADERSHIP', 'Jamal Mammadrzayev', 'Джамал Мамедрзаев');
INSERT INTO `team_members` VALUES ('12', null, '2020-05-20 13:21:13', '2020-05-20 13:21:13', 'Elşən Zeynalov', '6', 'elshan.zeynalov@pmis.az', 'https://www.linkedin.com/mwlite/in/elshan-zeynalov-680b4514b', 'Baş Planlama mühəndisi', 'Lead Planning Engineer', 'Ведущий инженер по планированию', 'OUR PEOPLE', 'Elshan Zeynalov', 'Эльшан Зейналов');
INSERT INTO `team_members` VALUES ('13', null, '2020-05-20 13:22:27', '2020-05-20 13:22:47', 'Fuad Nəcəfov', '6', 'fuad.najafov@pmis.az', '', 'Tikinti və İstehsalatda İş Həcmlərinə Nəzarətçi Mühəndis', ' Quantity Surveyor', 'Инженер по контролю объема строительства и производства', 'OUR PEOPLE', 'Fuad Najafov', 'Фуад Наджафов');
INSERT INTO `team_members` VALUES ('14', null, '2020-05-20 13:27:18', '2020-05-20 13:27:18', 'Tural Hüseynov', '6', '', 'https://www.linkedin.com/in/tural-huseynov-b741b4a', 'Biznes Xidmətləri üzrə Məsləhətçi  / Böyük Britaniya , London', 'Project Services Advisor /  UK , London', 'Консультант по вопросам Коммерческих Услуг / Великобритания , Лондон', 'OUR EXPERT WITNESSES', 'Tural Huseynov', 'Турал Гусейнов');

-- ----------------------------
-- Table structure for timekeeperings
-- ----------------------------
DROP TABLE IF EXISTS `timekeeperings`;
CREATE TABLE `timekeeperings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  `text_az` text COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of timekeeperings
-- ----------------------------
INSERT INTO `timekeeperings` VALUES ('1', null, '2020-05-05 04:29:06', '2020-05-05 04:29:06', '13', '', '<p class=\"MsoNormal\">Project Management.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">PMIS Project Management team\r\ncomprise a group of dedicated and experienced project management professionals\r\ndelivering high quality schemes across a selection of Client sectors throughout\r\nAzerbaijan.</span><span style=\"font-size:9.0pt;line-height:107%;font-family:\r\n\"Arial\",sans-serif;color:black\"><br>\r\n<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Our team provide Project Management and Contract\r\nAdministration services in relation to greenfield and brownfield turnkey\r\nprojects from Strategic planning through to project delivery. Our aim is to add\r\nvalue and manage risk by working with Clients with our transactional teams to\r\nprovide a comprehensive “one stop” capability.<o:p></o:p></span></span></p>', '');

-- ----------------------------
-- Table structure for uploads
-- ----------------------------
DROP TABLE IF EXISTS `uploads`;
CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT 1,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploads_user_id_foreign` (`user_id`),
  CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of uploads
-- ----------------------------
INSERT INTO `uploads` VALUES ('1', 'WhatsApp Image 2020-04-27 at 15.25.08.jpeg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-01-130251-WhatsApp Image 2020-04-27 at 15.25.08.jpeg', 'jpeg', '', '1', 'jz85ixgm2jvniq2qivye', '0', null, '2020-05-01 13:02:51', '2020-05-03 11:32:54');
INSERT INTO `uploads` VALUES ('2', 'emil.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-01-203356-emil.jpg', 'jpg', '', '1', 'ttqjtrt9httuxehhmthf', '0', '2020-05-03 12:44:27', '2020-05-01 20:33:56', '2020-05-03 12:44:27');
INSERT INTO `uploads` VALUES ('3', 'logo.png', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-01-203420-logo.png', 'png', '', '1', '0xkei0iu6mhlqjv72bd3', '1', null, '2020-05-01 20:34:20', '2020-05-01 20:34:20');
INSERT INTO `uploads` VALUES ('4', 'WhatsApp Image 2020-04-27 at 15.02.32.jpeg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-03-112909-WhatsApp Image 2020-04-27 at 15.02.32.jpeg', 'jpeg', '', '1', 'adwy7ljel2lp89tmd0vg', '0', '2020-05-03 11:33:21', '2020-05-03 11:29:09', '2020-05-03 11:33:21');
INSERT INTO `uploads` VALUES ('5', 'WhatsApp Image 2020-04-27 at 15.02.32.jpeg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-03-113332-WhatsApp Image 2020-04-27 at 15.02.32.jpeg', 'jpeg', '', '1', '6bnf4uypm9mvyhweb6v1', '0', null, '2020-05-03 11:33:32', '2020-05-03 11:33:32');
INSERT INTO `uploads` VALUES ('6', 'emil.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-03-124528-emil.jpg', 'jpg', '', '1', 'hf5jrqq6mv73nxwwk65s', '1', null, '2020-05-03 12:45:28', '2020-05-03 12:45:28');
INSERT INTO `uploads` VALUES ('7', '637966.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-03-135229-637966.jpg', 'jpg', '', '1', '7xvbp4ngbmq7owtwn1lz', '1', null, '2020-05-03 13:52:29', '2020-05-03 13:52:29');
INSERT INTO `uploads` VALUES ('8', '178440.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-04-190824-178440.jpg', 'jpg', '', '1', 'ktqqvb1kaolji0pjelzm', '1', null, '2020-05-04 19:08:24', '2020-05-04 19:08:24');
INSERT INTO `uploads` VALUES ('9', 'helpbox-contact.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-05-042526-helpbox-contact.jpg', 'jpg', '', '1', '5jpyfxmurrtfmgr0pmcx', '1', null, '2020-05-05 04:25:26', '2020-05-05 04:25:26');
INSERT INTO `uploads` VALUES ('10', 'w1920 (1).jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-05-042549-w1920 (1).jpg', 'jpg', '', '1', 'rjr5imnshfh4ngkxa1xo', '1', null, '2020-05-05 04:25:49', '2020-05-05 04:25:49');
INSERT INTO `uploads` VALUES ('11', 'w1920 (2).jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-05-042549-w1920 (2).jpg', 'jpg', '', '1', 'yxw1p0bt2qfeoclfhso1', '1', null, '2020-05-05 04:25:49', '2020-05-05 04:25:49');
INSERT INTO `uploads` VALUES ('12', 'w1920 (3).jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-05-042550-w1920 (3).jpg', 'jpg', '', '1', '2qnwrbmba25qpegm6wto', '1', null, '2020-05-05 04:25:50', '2020-05-05 04:25:50');
INSERT INTO `uploads` VALUES ('13', 'w1920 (4).jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-05-042550-w1920 (4).jpg', 'jpg', '', '1', 'zku0hj2bvpqykuep7uws', '1', null, '2020-05-05 04:25:50', '2020-05-05 04:25:50');
INSERT INTO `uploads` VALUES ('14', 'w1920.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-05-042553-w1920.jpg', 'jpg', '', '1', 'ozyzlehrgpw7okiem4mr', '1', null, '2020-05-05 04:25:53', '2020-05-05 04:25:53');
INSERT INTO `uploads` VALUES ('15', '2020-02-14-180450-035-617-16-051-00_340_auto_5_80.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-06-182739-2020-02-14-180450-035-617-16-051-00_340_auto_5_80.jpg', 'jpg', '', '1', 'ez5wdxtzuv5q9c8ngssd', '1', null, '2020-05-06 18:27:39', '2020-05-06 18:27:39');
INSERT INTO `uploads` VALUES ('16', '2020-02-12-172858-alexander-popov-3InMDrsuYrk-unsplash.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-06-182740-2020-02-12-172858-alexander-popov-3InMDrsuYrk-unsplash.jpg', 'jpg', '', '1', 'vsms58cdu2wg5yda61pz', '1', null, '2020-05-06 18:27:40', '2020-05-06 18:27:40');
INSERT INTO `uploads` VALUES ('17', 'logo (2).png', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\pmis\\storage\\uploads\\2020-05-16-121708-logo (2).png', 'png', '', '1', '9uvpw6lcqj27a6gawteb', '1', null, '2020-05-16 12:17:08', '2020-05-16 12:17:08');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(10) unsigned NOT NULL DEFAULT 0,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Super Admin', '1', 'user@example.com', '$2y$10$TW/wGL1.Ja.6J719SZXi4.t3yKRCGcIpeJUtnp0awBA3KANKWjjgy', 'Employee', 'zsTkuOPrfcBn7kXDp4twOMnpKBrvQfdQd7PJnSpQye9i1tVMCXWvrJXzkCHR', null, '2020-05-01 10:35:27', '2020-05-01 21:25:23');

-- ----------------------------
-- Table structure for vacancies
-- ----------------------------
DROP TABLE IF EXISTS `vacancies`;
CREATE TABLE `vacancies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `location_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `location_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `location_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `education_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `education_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `education_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `experience_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `experience_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `experience_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `salary` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_az` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_en` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_ru` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `published_on` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `expired_on` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `meal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_az` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `requirements_az` text COLLATE utf8_unicode_ci NOT NULL,
  `requirements_en` text COLLATE utf8_unicode_ci NOT NULL,
  `requirements_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `transport` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vacancies
-- ----------------------------
INSERT INTO `vacancies` VALUES ('1', null, '2020-05-19 08:19:56', '2020-05-19 13:57:36', 'programci', 'programmer', 'programist', 'baku', 'london', 'moskva', 'programci', 'programmer', 'programist', '18-25', 'bakalavr', 'bachelor', 'bakalavr', '3 il', '3 year', '3 goda', '1999 azn', '10-18', '10-19', '10-20', '01.12.18', '01.12.18', 'yes', '■ Development of high-performing and individually designed applications for our customers (B2C and B2B Shops)\r\n\r\n■ Implementation of technical concepts and software applications\r\n\r\n■ Identification of interfaces between B2C/B2B online shops and existing merchandise management systems on our client’s website\r\n\r\n■ Direct communication with the technical service providers of these merchandise management systems\r\n\r\n■ Performance optimization of all import interfaces\r\n\r\nWhat we expect:\r\n\r\n■ Proactive and innovative way of programming\r\n\r\n■ High expertise in object-oriented programming with PHP\r\n\r\n■ Previous experience with the creation of websites based on the LAMP technology (Linux/Apache/MySQL/PHP)\r\n\r\n■ Previous practical experience with the eShop System of Shopware (or Magento), as well as the development of Shopware plugins and templating with Smarty\r\n\r\n■ Knowledge of HTML5, CSS3 and jQuery', '■ Development of high-performing and individually designed applications for our customers (B2C and B2B Shops)\r\n\r\n■ Implementation of technical concepts and software applications\r\n\r\n■ Identification of interfaces between B2C/B2B online shops and existing merchandise management systems on our client’s website\r\n\r\n■ Direct communication with the technical service providers of these merchandise management systems\r\n\r\n■ Performance optimization of all import interfaces\r\n\r\nWhat we expect:\r\n\r\n■ Proactive and innovative way of programming\r\n\r\n■ High expertise in object-oriented programming with PHP\r\n\r\n■ Previous experience with the creation of websites based on the LAMP technology (Linux/Apache/MySQL/PHP)\r\n\r\n■ Previous practical experience with the eShop System of Shopware (or Magento), as well as the development of Shopware plugins and templating with Smarty\r\n\r\n■ Knowledge of HTML5, CSS3 and jQuery', '■ Development of high-performing and individually designed applications for our customers (B2C and B2B Shops)\r\n\r\n■ Implementation of technical concepts and software applications\r\n\r\n■ Identification of interfaces between B2C/B2B online shops and existing merchandise management systems on our client’s website\r\n\r\n■ Direct communication with the technical service providers of these merchandise management systems\r\n\r\n■ Performance optimization of all import interfaces\r\n\r\nWhat we expect:\r\n\r\n■ Proactive and innovative way of programming\r\n\r\n■ High expertise in object-oriented programming with PHP\r\n\r\n■ Previous experience with the creation of websites based on the LAMP technology (Linux/Apache/MySQL/PHP)\r\n\r\n■ Previous practical experience with the eShop System of Shopware (or Magento), as well as the development of Shopware plugins and templating with Smarty\r\n\r\n■ Knowledge of HTML5, CSS3 and jQuery', 'telebler', 'requirement', 'trebovaniya', 'yes');
INSERT INTO `vacancies` VALUES ('2', null, '2020-05-19 08:19:56', '2020-05-19 08:23:14', 'programci', 'programmer', 'programist', 'baku', 'london', 'moskva', 'programci', 'programmer', 'programist', '18-25', 'bakalavr', 'bachelor', 'bakalavr', '3 il', '3 year', '3 goda', '1999 azn', '10-18', '10-19', '10-20', '01.12.18', '01.12.18', 'yes', 'izah', 'description', 'opisanie', 'telebler', 'requirement', 'trebovaniya', 'yes');
INSERT INTO `vacancies` VALUES ('3', null, '2020-05-19 08:23:14', '0000-00-00 00:00:00', 'programmer', 'programist', 'baku', 'london', 'moskva', 'programci', 'programmer', 'programist', '18-25', 'bakalavr', 'bachelor', 'bakalavr', '3 il', '3 year', '3 goda', '1999 azn', '10-18', '10-19', '10-20', '01.12.18', '', '01.12.18', 'izah', 'description', 'opisanie', 'telebler', 'requirement', 'trebovaniya', 'yes', '');
INSERT INTO `vacancies` VALUES ('4', null, '2020-05-19 08:23:14', '0000-00-00 00:00:00', 'programmer', 'programist', 'baku', 'london', 'moskva', 'programci', 'programmer', 'programist', '18-25', 'bakalavr', 'bachelor', 'bakalavr', '3 il', '3 year', '3 goda', '1999 azn', '10-18', '10-19', '10-20', '01.12.18', '', '01.12.18', 'izah', 'description', 'opisanie', 'telebler', 'requirement', 'trebovaniya', 'yes', '');
INSERT INTO `vacancies` VALUES ('5', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'programist', 'baku', 'london', 'moskva', 'programci', 'programmer', 'programist', '18-25', 'bakalavr', 'bachelor', 'bakalavr', '3 il', '3 year', '3 goda', '1999 azn', '10-18', '10-19', '10-20', '01.12.18', '', 'yes', '01.12.18\r\n', 'description', 'opisanie', 'telebler', 'requirement', 'trebovaniya', 'yes', '', '');

-- ----------------------------
-- Table structure for vacancy_images
-- ----------------------------
DROP TABLE IF EXISTS `vacancy_images`;
CREATE TABLE `vacancy_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vacancy_images
-- ----------------------------
INSERT INTO `vacancy_images` VALUES ('1', null, '2020-05-19 08:59:24', '2020-05-19 08:59:24', '12');
