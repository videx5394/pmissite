<?php


namespace App;


use Illuminate\Support\Facades\DB;

class Page
{
    public static function getVacancies()
    {
        $locale = \request()->get('locale');
        $image = DB::select("SELECT CONCAT(uploads.hash,'/',uploads.name) as image_path FROM `vacancy_images`  LEFT JOIN uploads ON vacancy_images.image=uploads.id");
        $vacancies = DB::select("SELECT
                                title_$locale as title,
                                location_$locale as location,
                                position_$locale as position,
                                age,
                                education_$locale as education,
                                experience_$locale as experience,
                                salary,
                                schedule_$locale as schedule,
                                published_on,
                                `expired_on`,
                                meal,
                                description_$locale as description,
                                requirements_$locale as requirements,
                                transport
                            FROM
                                `vacancies` WHERE `vacancies`.deleted_at is null");

        $data = [
            'image' => '',
            'vacancies' => $vacancies
        ];

        if(count($image)>0)
        {
            $data['image_path'] = $image[0]->image_path;
        }

        return (object)$data;

    }

    public static function getProject($id)
    {
        $id = (int)$id;
        $locale = \request()->get('locale');
        $image = DB::select("SELECT CONCAT(uploads.hash,'/',uploads.name) as image_path FROM `project_images`  LEFT JOIN uploads ON project_images.image=uploads.id");
        $project = DB::select("SELECT
                                projects.id,
                                CONCAT(u1.hash,'/',u1.name) as image1_path,
                                CONCAT(u2.hash,'/',u2.name) as image2_path,
                                CONCAT(u3.hash,'/',u3.name) as image3_path,
                                CONCAT(u4.hash,'/',u4.name) as image4_path,
                                project_title_$locale as title,
                                location_$locale as location,
                                date_$locale as `date`,
                                text_$locale as `text`
                            FROM
                                `projects`  
                                LEFT JOIN uploads u1 ON projects.image1=u1.id 
                                LEFT JOIN uploads u2 ON projects.image2=u2.id 
                                LEFT JOIN uploads u3 ON projects.image3=u3.id 
                                LEFT JOIN uploads u4 ON projects.image4=u4.id 
                                WHERE `projects`.deleted_at is null AND projects.id = $id");

        $data = [
            'image_path' => '',
            'project' => $project
        ];

        if(count($image)>0)
        {
            $data['image_path'] = $image[0]->image_path;
        }

        return (object)$data;

    }

    public static function getProjects()
    {
        $locale = \request()->get('locale');
        $image = DB::select("SELECT CONCAT(uploads.hash,'/',uploads.name) as image_path FROM `project_images`  LEFT JOIN uploads ON project_images.image=uploads.id");
        $projects = DB::select("SELECT
                                projects.id,
                                CONCAT(uploads.hash,'/',uploads.name) as small_image_path,
                                project_title_$locale as title,
                                location_$locale as location,
                                date_$locale as `date`
                            FROM
                                `projects`  LEFT JOIN uploads ON projects.small_image=uploads.id WHERE `projects`.deleted_at is null");

        $data = [
            'image_path' => '',
            'projects' => $projects
        ];

        if(count($image)>0)
        {
            $data['image_path'] = $image[0]->image_path;
        }

        return (object)$data;

    }

    public static function getOurTeamData()
    {
        $locale = \request()->get('locale');
        $images =  DB::select("SELECT 
                                            CONCAT(uploads.hash,'/',uploads.name) as image_path,
                                            CONCAT(u2.hash,'/',u2.name) as leadership_image_path, 
                                            CONCAT(u3.hash,'/',u3.name) as people_image_path ,
                                            CONCAT(u1.hash,'/',u1.name) as expert_image_path 
                                            FROM our_team_images 
                                            LEFT JOIN uploads ON our_team_images.image=uploads.id
                                            LEFT JOIN uploads as u1 ON our_team_images.expert_image=u1.id
                                            LEFT JOIN uploads as u2 ON our_team_images.leadership_image=u2.id
                                            LEFT JOIN uploads as u3 ON our_team_images.people_image=u3.id");

        $members = DB::select("SELECT team_members.*,team_members.name_$locale as 'name',team_members.position_$locale as 'position',CONCAT(uploads.hash,'/',uploads.name) as image_path from team_members LEFT JOIN uploads ON team_members.profile_image=uploads.id WHERE team_members.deleted_at IS null");

        if(count($images)>0)
        {
            $image_path = $images[0]->image_path;
            $leadership_image_path = $images[0]->leadership_image_path;
            $people_image_path = $images[0]->people_image_path;
            $expert_image_path = $images[0]->expert_image_path;
        }
        else
        {
            $image_path = '';
            $leadership_image_path = '';
            $people_image_path = '';
            $expert_image_path = '';
        }


        return (object)[
            'image_path' => $image_path,
            'leadership_image_path' => $leadership_image_path,
            'people_image_path' => $people_image_path,
            'expert_image_path' => $expert_image_path,
            'members' => $members
        ];

    }

    public static function getHomeData()
    {
        $locale = \request()->get('locale');
        $banner_data_query = "SELECT
                                            CONCAT(uploads.hash,'/',uploads.name) as image_path,
                                            homes.text_$locale as text,
                                            homes.slogan_$locale as slogan
                                            FROM homes 
                                            LEFT JOIN uploads ON homes.image=uploads.id";

        $about_data_query = "SELECT 
                                            about_us_shorts.text_$locale as text
                                            FROM about_us_shorts ";

        $clients_data_query = "SELECT CONCAT(uploads.hash,'/',uploads.name) as image_path,
                                            clients.text_$locale as text
                                            FROM clients 
                                            LEFT JOIN uploads ON clients.logo=uploads.id
                                            WHERE clients.deleted_at is null AND uploads.deleted_at is null";

        $banner_data = DB::select($banner_data_query);
        $about_data = DB::select($about_data_query);
        $clients_data = DB::select($clients_data_query);

        $data = (object)[
            'banner' => $banner_data[0],
            'about' => $about_data[0],
            'clients' => $clients_data,
        ];
        return $data;
    }



    public static function getPageData(string $table_name,$hasSlogan = false)
    {
        $locale = \request()->get('locale');
        $query = "SELECT $table_name.id,$table_name.text_$locale as text ".($hasSlogan ? " , $table_name.slogan_$locale as slogan" : "").",CONCAT(uploads.hash,'/',uploads.name) as image_path from $table_name LEFT JOIN uploads ON $table_name.image=uploads.id";
        $result = DB::select($query);
        if(count($result)>0)
        {
            return $result[0];
        }
        else
        {
            $object = new \stdClass();
            $object->id = 0;
            $object->text = '';
            $object->slogan = '';
            $object->image_path = '';

            return $object;
        }
        return $query;
    }
}
