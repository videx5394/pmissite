<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class ContactInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $infos =  DB::select('SELECT email,phone,linkedin FROM contact_infos LIMIT 1');

        $request->contact_info = [
            'phone' =>  config('app.contact_phone'),
            'email' => config('app.contact_mail'),
            'linkedin'=> ''
        ];

        if(count($infos)>0)
        {
            $request->session()->put('contact_info', [
                'phone' => $infos[0]->phone,
                'email' => $infos[0]->email,
                'linkedin'=> $infos[0]->linkedin
            ]);



            $request->contact_info = [
                'phone' => $infos[0]->phone,
                'email' => $infos[0]->email,
                'linkedin'=> $infos[0]->linkedin
            ];
        }

        return $next($request);
    }
}
