<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class contactUsMail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'subject' => 'required|max:255',
            'comment' => 'required|max:5000',
            'email' => 'required|max:100|email'
        ];
    }
}
