<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageController extends Controller
{
    public $fallback_locale;

    public function __construct()
    {
        $this->fallback_locale = config('app.fallback_locale');
    }

    public function index(Request $request, $lang = false)
    {
        if ($lang == false || !in_array($lang, ['az','ru','en'])) {
            $lang = $this->fallback_locale;
        }
        $request->session()->forget('locale');
        $request->session()->put('locale', $lang);
        return redirect()->back();
    }
}
