<?php

namespace App\Http\Controllers;

use App\Http\Requests\contactUsMail;
use App\Models\Home;
use App\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $data = Page::getHomeData();
        return view('home',['data'=>$data]);
    }
    public function about_us(){
        $data = Page::getPageData('abouts');
        return view('standardPage',['data'=>$data,'page_name'=>'about_us']);
    }
    public function our_team(){
        $data = Page::getOurTeamData();
        return view('ourTeam',['data' => $data,'page_name'=>'our_team']);
    }

    public function vacancy(){
        $data = Page::getVacancies();
        return view('vacancies',['data' => $data,'page_name'=>'vacancy']);
    }

    public function projects(){
        $data = Page::getProjects();
        return view('projects',['data' => $data,'page_name'=>'projects']);
    }

    public function applyVacancy(){
        $position = request()->input('position');
        if(request()->hasFile('cv') && is_string($position))
        {
            $filenameWithExt    = request()->file('cv')->getClientOriginalName();
            $filename           = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension          = request()->file('cv')->getClientOriginalExtension();
            $mailResult = \App\Home::applyVacancyMail($position,request()->file('cv'),$filename);

            if(!$mailResult)
                return response(['message'=>'mail is not sent'], 400);
        }
        return 'ok';
    }


    public function project(Request $request,$id)
    {
        $data = Page::getProject($id);
        if(count($data->project)==0)
        {
            return redirect()->back();
        }

        $data->project = (object)$data->project[0];

        //dd($data);

        return view('project',['data' => $data,'page_name'=>'projects']);
    }



    public function our_leadership(){
        $data = Page::getOurTeamData();
        return view('ourLeadership',['data' => $data,'page_name'=>'our_leadership']);
    }
    public function our_people(){
        $data = Page::getOurTeamData();
        return view('ourPeople',['data' => $data,'page_name'=>'our_people']);
    }
    public function our_expert_witnesses(){
        $data = Page::getOurTeamData();
        return view('ourExpertWitnesses',['data' => $data,'page_name'=>'our_expert_witnesses']);
    }
    public function contact_us(){
        $data = Page::getPageData('contact_uses');
        return view('contactUs',['data' => $data,'page_name'=>'contact_us']);
    }

    //contact_us

    public function our_clients(){
        return view('home');
    }
    // dodelat
    public function PSS(){
        $data = Page::getPageData('project_serv_sols');
        return view('standardPage',['data'=>$data,'page_name'=>'PSS']);
    }
    public function PSACE(){
        $data = Page::getPageData('prop_sp_and_cost_ests',true);
        return view('standardPage',['data'=>$data,'page_name'=>'PSACE']);
    }

    public function PAS(){
        $data = Page::getPageData('planning_and_scheds',true);
        return view('standardPage',['data'=>$data,'page_name'=>'PAS']);
    }
    public function QSACA(){
        $data = Page::getPageData('quality_s_and_con_ads',true);
        return view('standardPage',['data'=>$data,'page_name'=>'QSACA']);
    }
    public function CCAS(){
        $data = Page::getPageData('cost_contr_and_subs',true);
        return view('standardPage',['data'=>$data,'page_name'=>'CCAS']);
    }
    public function timekeeping(){
        $data = Page::getPageData('timekeeperings',true);
        return view('standardPage',['data'=>$data,'page_name'=>'timekeeping']);
    }
    public function HDA_benchmarking(){
        $data = Page::getPageData('benchmarkings');
        return view('standardPage',['data'=>$data,'page_name'=>'HDA_benchmarking']);
    }
    public function RAA(){
        $data = Page::getPageData('reporting_and_autos',true);
        return view('standardPage',['data'=>$data,'page_name'=>'RAA']);
    }
    public function contactUsMail(contactUsMail $request)
    {
        $validated = $request->validated();

        if(isset($validated['errors']))
        {
            return response()->json($validated['errors']);
        }
        $mailResult = \App\Home::contactUsMail($request);

        if(!$mailResult)
            return response(['message'=>'mail is not sent'], 400);
    }
}
