<?php


namespace App;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Home
{
    public static function contactUsMail($request)
    {
        try {
            $to_name = $request->session()->get('contact_info')['phone'];
            $to_email = $request->session()->get('contact_info')['email'];

            $data = [
                'request' => $request
            ];

            Mail::send("contactUsMail", $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject("Contact Us");
                $message->from(config('app.from_mail'), "Contact Us");
            });

            if (Mail::failures()) {
                return false;
                // return response showing failed emails
            }
            return true;
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return false;
        }
    }
    public static function applyVacancyMail($position,$cv,$cvName)
    {
        try {
            $to_name = session()->get('contact_info')['phone'];
            $to_email = 'vacancy@pmis.az';

            $data = [
                'position' => $position,
                'cv' => $cv
            ];

            Mail::send("applyVacancyMail", $data, function ($message) use ($to_name, $to_email,$cv,$cvName) {
                $message->to($to_email, $to_name)
                    ->subject("Vacancy");
                $message->attach($cv->getRealPath(),
                    [
                        'as' => $cvName,
                        'mime' => $cv->getClientMimeType(),
                    ]);
                $message->from(config('app.from_mail'), "Vacancy");
            });

            if (Mail::failures()) {
                return false;
                // return response showing failed emails
            }
            return true;
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return false;
        }
    }
}
