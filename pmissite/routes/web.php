<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});


Route::get('/lang/{lang}', 'LanguageController@index')->name('language');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about_us', 'HomeController@about_us')->name('about_us');
Route::get('/our_team', 'HomeController@our_team')->name('our_team');
Route::get('/our_leadership', 'HomeController@our_leadership')->name('our_leadership');
Route::get('/our_people', 'HomeController@our_people')->name('our_people');
Route::get('/our_expert_witnesses', 'HomeController@our_expert_witnesses')->name('our_expert_witnesses');
Route::get('/our_clients', 'HomeController@our_clients')->name('our_clients');
Route::get('/contact_us', 'HomeController@contact_us')->name('contact_us');
Route::post('/contactUsMail', 'HomeController@contactUsMail')->name('contactUsMail');
Route::get('/PSACE', 'HomeController@PSACE')->name('PSACE');
Route::get('/PSS', 'HomeController@PSS')->name('PSS');
Route::get('/PAS', 'HomeController@PAS')->name('PAS');
Route::get('/QSACA', 'HomeController@QSACA')->name('QSACA');
Route::get('/CCAS', 'HomeController@CCAS')->name('CCAS');
Route::get('/timekeeping', 'HomeController@timekeeping')->name('timekeeping');
Route::get('/RAA', 'HomeController@RAA')->name('RAA');
Route::get('/projects', 'HomeController@projects')->name('projects');
Route::get('/project/{id}', 'HomeController@project')->name('project');
Route::get('/vacancy', 'HomeController@vacancy')->name('vacancy');
Route::get('/contact_us', 'HomeController@contact_us')->name('contact_us');
Route::post('/applyVacancy', 'HomeController@applyVacancy')->name('applyVacancy');
